******************************
Python Tools cho Visual Studio
******************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2011-08-31 09:37:00
:tags: ide


Ngày 29 tháng 08 vừa qua, Microsoft đã tung ra phiên bản đầu tiên của phần mềm miễn phí, nguồn mở `Python Tools <http://pytools.codeplex.com/>`_ dành cho Visual Studio.

Với công cụ này, người dùng sẽ sử dụng giao diện quen thuộc của Visual Studio để viết mã theo ngôn ngữ Python. Một số tính năng chính bao gồm trình soạn thảo, quản lý dự án, trình gỡ rối, hỗ trợ Intellisense và Refactoring, hỗ trợ IronPython lẫn CPython. Ngoài ra, nếu sử dụng Visual Studio phiên bản Ultimate, người dùng sẽ có thêm cả trình Profiler.

Công cụ Python Tools cho Visual Studio cần phiên bản `Visual Studio Shell <http://www.microsoft.com/downloads/en/details.aspx?familyid=8E5AA7B6-8436-43F0-B778-00C3BCA733D3&displaylang=en>`_ miễn phí, hoặc Visual Studio phiên bản Pro trở lên. Công cụ này không sử dụng chung với Visual Studio Express được.

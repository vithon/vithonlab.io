************
Cuộc thi Đời
************


:author: Nhóm PCNV
:category: Tin tức
:date: 2011-09-15 06:36:00
:tags: python, game, contest


Nhóm PCNV hân hạnh tổ chức một cuộc thi nhỏ với giải thưởng lớn!

Game of Life, tạm dịch Trò chơi Đời, là một mô phỏng trạng thái các điểm trên một ma trận MxN đơn giản. Các điểm trên ma trận có trạng thái hoặc là sống, hoặc là chết. Trạng thái của một ma trận được gọi là một thế hệ. Các điểm trên ma trận ở thế hệ **G** sẽ tuân theo các luật sau để xác định trạng thái của chúng ở thế hệ **G + 1**:

 #. Nếu số lượng các điểm sống chung quanh điểm hiện tại là 3, thì điểm hiện tại sẽ sống.
 #. Nếu số lượng các điểm sống chung quanh điểm hiện tại là 2, thì điểm hiện tại sẽ tiếp tục giữ trạng thái hiện tại.
 #. Nếu số lượng các điểm sống chung quanh điểm hiện tại nhỏ hơn 2, hoặc lớn hơn 3, điểm hiện tại sẽ chết.

**Cuộc thi Đời** là sự tranh đua của các mã nguồn Python thực hiện việc tối ưu hóa cách thể hiện mô phỏng trên. Thể lệ của cuộc thi bao gồm:

 #. Bài tham dự gửi về địa chỉ thư admin tại vithon.org.
 #. Bài tham dự chỉ được sử dụng ngôn ngữ Python, và các thư viện chuẩn đi kèm.
 #. Bài tham dự chỉ cần hiện thực hóa lớp mẫu bên dưới bằng cách kế thừa nó và cài đặt các phương thức cần thiết.
 #. Yếu tố cơ bản để đánh giá là tốc độ chạy của chương trình.
 #. Hạn tham dự là hết ngày 15 tháng 10 năm 2011.
 #. **Chỉ có một** giải có trị giá **tương đương 250 đô la Singapore**.
 #. Quyết định của ban tổ chức là cuối cùng, xin không nhận khiếu nại.

.. code-block:: python

  class Board(object):
    '''Represents the Game of Life matrix.

    Each cell has at most 8 neighbors. A cell may have less than 8 neighbors.
    For example, a cell at (0, 0) would have only three neightbors (0, 1),
    (1, 0) and (1, 1).

    The cells are addressed in a row-major (row first) manner. The index starts
    from zero.

    '''

    def __init__(self, width=10, height=10):
        '''Initializes this board.

        Args:
            width (int): Number of cells in a row.
            height (int): Number of rows in this board.

        '''

        raise NotImplemented()

    def generation(self, n=1):
        '''Transform this board into next ``n`` generation(s).

        The transformation is in place similar to ``list.sort``.
        No value is returned to the caller.

        Args:
            n (int): Number of generations.

        Raises:
            ValueError: If n is smaller than 1.

        '''

        raise NotImplemented()

    def get_cell(self, row, col):
        '''Returns the state of the cell at row ``row`` and column ``col``.

        Args:
            row (int): The row, zero-indexed.
            col (int): The column, zero-indexed.

        Returns:
            True: If the cell is alive.
            False: If the cell is dead.

        '''

        raise NotImplemented()

    def set_cell(self, row, col, state):
        '''Sets a cell at row ``row`` and column ``col`` to state ``state``.

        Args:
            row (int): The row, zero-indexed.
            col (int): The column, zero-indexed.
            state (bool): Cell's state.

        '''

        raise NotImplemented()

    def get_row(self, row):
        '''Returns the states of all cells in row ``row``.

        Args:
            row (int): The row, zero-indexed.

        Returns:
            A sequence of bools.

        '''

        raise NotImplemented()

Mọi thắc mắc và thảo luận xin vui lòng gửi lên `diễn đàn <forum.vithon.org>`_.
********************************
Python S60 cho người mới bắt đầu
********************************


:author: Trịnh Thành Trung
:category: Bài viết
:date: 2007-08-18 12:54:00
:tags: dummy, s60


Xin chào các bạn, mình là thành viên mới của forum. Mình thực sự rất yêu thích Python, và cũng mới bắt đầu nghiên cứu về Python trong thời gian gần đây. Đặc biệt mình có nghiên cứu thêm về phát triển ứng dụng Python cho điện thoại Nokia S60. Trong quá trình học mình có ghi chép lại những gì đã học thành tài liệu Python S60 cho người mới bắt đầu. Tài liệu này mình vẫn viết, được cập nhật hàng ngày và có thể xem cũng như download tại http://my.opera.com/noname00/forums/topic.dml?id=198943 (Khuyến cáo nên down bản PDF, vì nếu có thay đổi thì bản PDF sẽ được thay đổi chứ mình không thay đổi trên forum :D)

Mình nghĩ nó thích hợp cho cả những người mới bắt đầu làm quen với Python (như mình) cũng như những người thích lập trình Python cho điện thoại di động. 

Mong các bạn cho ý kiến

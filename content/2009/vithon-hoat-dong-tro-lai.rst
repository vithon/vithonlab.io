************************
Vithon hoạt động trở lại
************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2009-11-25 07:52:00
:tags: vithon


Trong vài ngày qua, vì máy chủ cần được nâng cấp nên trang mạng vithon.org đã tạm ngừng hoạt động. Trong khi chờ đợi máy chủ nâng cấp hoàn thành, trang vithon.org đã được di dời qua một máy chủ mới.

Các bài viết và thảo luận diễn đàn đã được sao lưu đầy đủ. Nhóm Python cho người Việt sẽ tải dần tất cả những bài viết và thảo luận diễn đàn lên trang mạng trong các ngày tới.

Các bạn thành viên cũ có quyền cập nhật trang mạng xin vui lòng liên lạc quản trị để cấp lại tài khoản trên hệ thống Zine này.

Chân thành cáo lỗi cùng các bạn vì sự bất tiện này.
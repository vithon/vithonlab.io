********************************
Cộng đồng PCNV hỗ trợ GNOME.Asia
********************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2009-10-27 20:13:00
:tags: community, gnome, asia


|GnomeAsia|_

.. |GnomeAsia| image:: /static/gnome.asia.2009.png
.. _GnomeAsia: http://gnome.asia

PCNV là một trong những nhóm hỗ trợ cộng đồng tại hội thảo GNOME Asia 2009 tổ chức tại Công viên Phần mềm Quang Trung, thành phố Hồ Chí Minh.
 
GNOME.Asia Summit diễn ra từ ngày 20 đến 22 tháng 11 năm 2009 tại Công viên phần mềm Quang Trung, thành phố Hồ Chí Minh, là một trong những sự kiện công nghệ nguồn mở hàng đầu trong khu vực, sẽ được đăng tải trên 20 tờ báo, đài truyền hình, và các diễn đàn trong và ngoài nước. Với mục đích khám phá tương lai của cộng đồng mã nguồn mở ở châu Á, và mở rộng cơ hội hợp tác Á-Âu, Gnome.Asia Summit dự kiến sẽ có sự tham gia của hơn 500 khách mời từ 10 quốc gia trên thế giới. Ban tổ chức trân trọng mời các cá nhân, đoàn thể có quan tâm cùng tham gia sự kiện này.

Ngoài việc kêu gọi cộng đồng người sử dụng Python tham gia vào hội thảo GNOME Asia, nhóm PCNV của chúng ta cũng sẽ tham gia hội thảo với bài trình bày *Python cho phần đời còn lại*. Đây cũng là một cơ hội để chúng ta cùng gặp mặt trực tiếp ngoài đời thường.
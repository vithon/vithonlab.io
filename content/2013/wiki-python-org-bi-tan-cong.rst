***************************
wiki.python.org bị tấn công
***************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2013-01-08 17:54:00
:tags: moin, security


Brian Curtin vừa gửi thông báo trên hộp thư chung **python-dev** với nội dung như sau:

  Vào ngày 28 tháng 12, một cá nhân chưa xác định được đã sử dụng một lỗi chưa công bố để tấn công http://wiki.python.org. Cá nhân này đã lấy được quyền truy cập như người dùng **moin** trên máy chủ, nhưng các dịch vụ khác vẫn chưa bị ảnh hưởng.

Mọi dữ liệu cá nhân cũng như mật khẩu tại http://wiki.python.org xem như đã nằm trong tay kẻ xấu. Nếu bạn có truy cập và tạo tài khoản trên trang http://wiki.python.org và sử dụng cùng mật khẩu ở các trang mạng khác thì hãy đổi ngay mật khẩu ở các trang mạng khác đó.

Thông tin cập nhật sẽ được Brian Curtin gửi trên **python-dev**.
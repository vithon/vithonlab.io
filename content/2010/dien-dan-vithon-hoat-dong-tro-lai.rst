*********************************
Diễn đàn vithon hoạt động trở lại
*********************************


:author: Phan Đắc Anh Huy
:category: Tin tức
:date: 2010-06-08 10:21:00
:tags: 


Chào mọi người, sau một thời gian gián đoạn, diễn đàn Vithon đã hoạt động trở lại tại địa chỉ `http://vithon.org/forum <http://vithon.org/forum>`_.

Phiên bản mới của diễn đàn do chính các thành viên của nhóm PCNV phát triển, được viết hoàn toàn bằng ngôn ngữ Python.

Mã nguồn của diễn đàn được host tại `BitBuket <http://bitbucket.org/vithon/vithon-forum>`_.

Hiện nay nhóm vẫn đang tiếp tục hoàn thiện các tính năng cơ bản và mở rộng cho diễn đàn. Chúng tôi chào đón mọi ý kiến đóng góp về tính năng sử dụng cũng như tham gia viết mã của tất cả các bạn.

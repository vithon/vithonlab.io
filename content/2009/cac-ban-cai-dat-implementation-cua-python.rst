*******************************************
Các bản cài đặt (implementation) của Python
*******************************************


:author: Võ Đức Phương
:category: Bài viết
:date: 2009-12-14 04:09:00
:tags: implementation


Có lẽ với nhiều người, khi nhắc tới Python sẽ chỉ nghĩ đến trình thông dịch được tải về từ trang web chính thức của ngôn ngữ này: https://www.python.org. Thế nhưng để đến với ngôn ngữ lập trình Python thì không chỉ có "con đường" đó. Là một chuẩn ngôn ngữ lập trình, Python có rất nhiều bản cài đặt khác nhau, với những mục tiêu cũng như những điểm mạnh, yếu khác nhau. Bài viết này sẽ giới thiệu một trong số những bản cài đặt đó, mong rằng nó sẽ hữu ích đối với những người muốn sử dụng Python nhưng vì một lí do nào đó mà không sử dụng được trình thông dịch chuẩn.

1- CPython
==========

Phiên bản mới nhất: 2.6 và 3.1

Đây chính là trình thông dịch chuẩn của Python, được coi là bản cài đặt tham chiếu cho ngôn ngữ này. Được viết bằng C và thực thi mã thông qua một trình thông dịch mã trung gian (bytecode interpreter). Là bản cài đặt chính thức, CPython có những tính năng mới nhất của Python, tuy nhiên bị hạn chế về một số mặt như tốc độ.

2- Jython
=========

Phiên bản mới nhất: 2.5

Được viết bằng Java, chương trình Python sử dụng các lớp của Java (Java class) thay vì mô đun của Python. Ngoài hầu hết các mô đun trong thư viện chuẩn Python, Jython có một ưu điểm rất lớn là có thể sử dụng các lớp trong thư viện chuẩn của Java. Mã lệnh của Jython không được biên dịch ra dạng Python bytecode như CPython mà sang dạng Java bytecode tương tự như các chương trình Java, và tất nhiên mã lệnh này phải được thực thi bởi máy ảo Java chứ không phải trình thông dịch như trong trường hợp của CPython.

3- IronPython
=============

Phiên bản mới nhất: 2.0, 2.6 (alpha)

Cũng tương tự như Jython, nhưng thay vì được biên dịch ra Java bytecode và thực thi bởi máy ảo Java, mã lệnh một chương trình IronPython được biên dịch sang dạng CIL (Common Intermediate Language) và thực thi bởi máy ảo .Net. Một chương trình viết bằng IronPython có thể sử dụng được các lớp của khung chương trình .Net, do vậy IronPython rất hữu dụng trong việc tích hợp vào một hệ thống viết bằng .Net.

4- Psyco
========

Phiên bản mới nhất: 1.6

Là một bản cài đặt đặc biệt sử dụng phương thức biên dịch tức thời (Just in time compiling) nhằm cải thiện tốc độ thực thi của Python. Tùy thuộc vào loại tác vụ mà Psyco có thể tăng tốc từ 1,5 đến hơn 40 lần.

5- PyPy
=======

Phiên bản mới nhất: 1.1

Tiền thân là Psyco, PyPy được viết trên một tập con các lệnh của Python (restricted Python - RPython) nhằm nâng cao tính đa nền tảng so với Psyco, ngoài ra việc sử dụng chính Python để viết khiến cho việc thay đổi và tối ưu đơn giản hơn. Cũng như người tiền nhiệm, PyPy là một trình biên dịch tức thời cho phép cải thiện tốc độ thực thi so với CPython. Tuy bắt đầu là một dự án mang tính nghiên cứu nhưng vì thấy được những ưu điểm của PyPy, hiện các tác giả của PyPy đang tập trung vào việc tương thích với CPython nhằm đưa PyPy vào sử dụng trong thực tiễn.

6- Unladen Swallow
==================

Phiên bản mới nhất: 2009Q3 (alpha)

Là một dự án đặc biệt của Google cũng nhằm tăng tốc độ thực thi của Python, nhưng khác với Psyco hay Pypy là dựa trên nền tảng LLVM. Unladen Swallow có mục đích là tăng ít nhất gấp 5 lần tốc độ của CPython. Điều này sẽ rất có lợi cho Google, do hãng này ứng dụng Python rất nhiều.

Ngoài những bản cài đặt ở trên, ngôn ngữ Python còn rất nhiều bản cài đặt khác như Stackless Python hay WPython v.v... tuy nhiên những bản cài đặt này thường có ứng dụng hẹp hơn, do vậy bài viết chỉ xin giới thiệu những bản cài đặt trên, mong rằng nếu như bạn không ưng ý với CPython thì giờ đây bạn có thể đến với Python theo một cách khác!
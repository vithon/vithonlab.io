****************************
Lập trình web với Python (3)
****************************


:author: Nguyễn Thành Nam
:category: Bài viết
:date: 2010-01-28 09:52:00
:tags: cgi, http


Sau khi đã cài đặt máy chủ web, và Python ở hai bài trước, trong bài này chúng ta sẽ bước vào thế giới lập trình web với mô hình đơn giản nhất, mô hình CGI.

CGI là viết tắt của Common Gateway Interface, dịch chính xác là giao tiếp cổng chung. Giao thức này được sinh ra để chuẩn hóa (hay xác định rõ) cách thức một máy chủ web giao phó việc tạo trang web cho các ứng dụng console (các ứng dụng nhận dữ liệu từ bộ nhập chuẩn và xuất ra bộ xuất chuẩn). Các ứng dụng này được gọi là các ứng dụng CGI, hay kịch bản CGI vì chúng thường được viết bằng ngôn ngữ kịch bản, mặc dù chúng cũng có thể được viết bằng các ngôn ngữ khác như C, Java.

Mô hình hoạt động
=================

Thông thường, một máy chủ web nhận yêu cầu từ máy khách và tìm kiếm trên hệ thống xem có tập tin mà máy khách cần truy xuất hay không. Nếu có thì máy chủ web sẽ xuất nội dung của tập tin này cho máy khách.

Trong giao thức CGI, máy chủ web nhận yêu cầu từ máy khách, tìm kiếm trên hệ thống xem có ứng dụng CGI đó không. Nếu có thì máy chủ web sẽ thực thi ứng dụng CGI, chuyển toàn bộ yêu cầu đến ứng dụng thông qua các biến môi trường và bộ nhập chuẩn. Ứng dụng CGI sẽ xử lý yêu cầu rồi gửi trả thông tin cho máy chủ web qua bộ xuất chuẩn. Máy chủ web sẽ nhận thông tin từ bộ xuất chuẩn của ứng dụng CGI để xuất trả lại cho máy khách. Mô hình hoạt động của giao thức CGI được miêu tả trong hình sau.

.. image:: /static/web-programming/cgi/cgi-model.png

Hello World
===========

Để hiểu rõ hơn cách hoạt động, chúng ta sẽ tạo một tập tin **helloworld.py** trong thư mục **C:\\Program Files\\Apache Software Foundation\\Apache2.2\\cgi-bin** với nội dung như sau:

.. code-block:: python

  #!c:/python26/python

  print "Content-Type: text/html"
  print ""
  print "Hello world."

Trước khi giải thích từng dòng lệnh, chúng ta sẽ xem xem kết quả đạt được là gì. Chúng ta sẽ dùng trình duyệt để vào trang **http://localhost/cgi-bin/helloworld.py**. Dĩ nhiên chúng ta cũng phải đảm bảo rằng máy chủ Apache đang chạy. Và đây là hình mà chúng ta nhận được.

.. image:: /static/web-programming/cgi/helloworld.png

Các yếu tố chính của chương trình CGI
=====================================

Như thế, chương trình CGI đầu tiên đã thực thi hoàn chỉnh. Hãy quay lại xem các yếu tố căn bản của một chương trình CGI viết bằng ngôn ngữ Python.

Dòng đầu tiên được gọi là dòng **shebang** với hai ký tự đặc biệt **#!**. Phần đi ngay sau hai ký tự này là đường dẫn tuyệt đối đến trình thông dịch Python trên hệ thống. Dòng này nói cho máy chủ web biết rằng tập tin này cần được đọc bởi trình thông dịch Python.

Dòng lệnh ``print`` thứ nhất cung cấp một đầu mục (header) cho máy chủ web. Đầu mục này là **Content-Type** với nội dung là **text/html**. Đây là đầu mục bắt buộc trong giao thức HTTP, do đó các ứng dụng CGI thông thường phải cung cấp đầu mục này.

Dòng lệnh in thứ hai (in ra một dòng trống) báo hiệu sự kết thúc của các đầu mục, và những gì theo sau sẽ là dữ liệu nội dung. Dòng lệnh này là bắt buộc.

Dòng lệnh in thứ ba xuất nội dung chính, là chuỗi **Hello world.**.

Các dòng lệnh này là những yếu tố chính cấu tạo nên một chương trình CGI viết bằng ngôn ngữ Python. Điểm quan trọng nhất mà chúng ta cần lưu ý là việc xuất dữ liệu thông qua bộ xuất chuẩn (stdout), và định dạng của dữ liệu cần xuất (đầu mục, dòng trống, nội dung).

Thêm vào các thẻ HTML
=====================

Chương trình của chúng ta khá buồn tẻ vì chúng ta khai báo **Content-Type** là **text/html** nhưng chúng ta không dùng thẻ HTML. Chúng ta hãy sửa lại chương trình như sau:

.. code-block:: python

  #!c:/python26/python

  print "Content-Type: text/html"
  print ""
  print """<html>
	<head>
		<title>Python!</title>
	</head>
	<body>
		<font size="+3">H</font>ello <font color="red">HTML</font>.
	</body>
  </html>"""

Quay lại trình duyệt và làm tươi (F5, hay Ctrl-R), chúng ta sẽ thấy hình như sau:

.. image:: /static/web-programming/cgi/hellohtml.png

Content-Type là quan trọng
==========================

Những gì chúng ta vừa thực hiện là thay đổi nội dung mà chúng ta xuất ra bộ xuất chuẩn.

Bây giờ, giữ cùng nội dung đấy, nhưng chúng ta thay đổi đầu mục **Content-Type** thành **text/plain**.

.. code-block:: python

  #!c:/python26/python

  print "Content-Type: text/plain"
  print ""
  print """<html>
	<head>
		<title>Python!</title>
	</head>
	<body>
		<font size="+3">H</font>ello <font color="red">HTML</font>.
	</body>
  </html>"""

Thử xem trang này với các trình duyệt **chuẩn** ví dụ Opera, hoặc Firefox thì chúng ta sẽ nhận được hình tương tự như sau:

.. image:: /static/web-programming/cgi/hellohtmlplain.png

Thay vì xử lý những thẻ HTML một cách đặc biệt thì trình duyệt đã không làm như vậy. Và đây là cách làm đúng bởi vì chúng ta nói cho trình duyệt biết rằng nội dung của chúng ta là ở dạng văn bản thường (plaintext) thông qua đầu mục **Content-Type** như trên. Trình duyệt IE không tuân theo chuẩn HTTP một cách triệt để nên vẫn sẽ hiển thị nội dung của chúng ta dưới dạng một trang HTML.

Một ít dữ liệu động
===================

Mục đích của chương trình CGI là xử lý và xuất dữ liệu nên sẽ là vô ích nếu chương trình CGI của chúng ta chỉ xuất các trang web tĩnh. Chúng ta sẽ sửa lại chương trình một chút như sau:

.. code-block:: python

  #!c:/python26/python

  import random

  actions = ("rocks", "rules", "kicks butt", "constricts camel")

  print "Content-Type: text/html"
  print ""
  print """<html>
	<head>
		<title>Python!</title>
	</head>
	<body>
		Python %s.
	</body>
  </html>""" % random.choice(actions)

Trong chương trình này, chúng ta sử dụng mô-đun **random** và hàm **choice** để chọn một hành động ngẫu nhiên. Khi làm tươi trình duyệt, chúng ta có thể gặp các hình tương tự như sau.

.. image:: /static/web-programming/cgi/dynamic1.png

.. image:: /static/web-programming/cgi/dynamic2.png

Tóm tắt
=======

Trong kỳ này chúng ta đã được giới thiệu về mô hình hoạt động của giao thức CGI, các yếu tố quan trọng trong một chương trình CGI viết bằng ngôn ngữ Python, một ít kiến thức về giao thức HTTP, và sự đa dạng của thế giới trình duyệt web. Ở kỳ sau chúng ta sẽ tìm hiểu cách nhận dữ liệu từ máy khách qua URL và mẫu đơn (form).
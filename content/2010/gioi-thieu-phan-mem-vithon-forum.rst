********************************
Giới thiệu phần mềm Vithon Forum
********************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2010-07-01 11:49:00
:tags: python, wsgi, vithon, forum, bulletin board


Nhóm Python Cho Người Việt (PCNV) hân hạnh công bố phần mềm **Vithon Forum** đến với cộng đồng.

Giới thiệu
==========

Phần mềm Vithon Forum (gọi tắt là **VF**) là một phần mềm tự do, mã mở, miễn phí với chức năng chính là một diễn đàn thảo luận tương tự như các gói phần mềm phpBB, punBB, pyForum.

VF là một ứng dụng web được viết bằng ngôn ngữ Python theo mô hình WSGI. VF có thể được triển khai với các máy chủ hỗ trợ WSGI hay CGI ví dụ như Apache. VF có thể sử dụng nhiều hệ cơ sở dữ liệu quan hệ như SQLite, MySQL, Oracle.

Tính năng
=========

VF có các tính năng nổi trội sau:

Đơn giản
  VF chỉ thực hiện một công việc chính đó là lưu trữ và hiển thị các thảo luận của người dùng một cách tiện lợi nhất. VF không mong muốn trở thành một công cụ tích hợp thư điện tử, lịch cá nhân, danh sách công việc, nhắn tin, tán gẫu, v.v...

Nhỏ gọn
  Vì chỉ phục vụ một số ít tác vụ cơ bản của một diễn đàn, toàn bộ chương trình VF (trừ các thư viện ngoài) chỉ chiếm khoảng 100 kilobyte dung lượng dĩa.

An toàn
  VF được phát triển với các tính năng an ninh được đặt lên hàng đầu. Vì sự đơn giản và tính nhỏ gọn của VF nên chương trình được thiết kế ưu tiên sự an toàn hơn tính năng, và việc kiểm tra mã nguồn được thực hiện thường xuyên nhằm đảm bảo sự ưu tiên đó.

Thuận tiện
  VF đề cao việc sử dụng các chuẩn mở như OpenID để việc sử dụng VF trở nên dễ dàng hơn cho người dùng. Bên canh đó, các tính năng thông thường mà người dùng đã quen thuộc ở các gói phần mềm khác cũng có mặt trong VF như BBCode. Tất cả chỉ nhằm một mục đích tạo ra môi trường thảo luận thuận tiện.

Yêu cầu hệ thống
================

Để triển khai VF, hệ thống sẽ cần một số ứng dụng và mô-đun sau:

* Python v2.5 trở lên.
* Mô-đun mako
* Mô-đun werkzeug
* Mô-đun sqlalchemy
* Mô-đun routes
* Mô-đun repoze.who.plugins.openid
* Mô-đun repoze.what.plugins.xml
* Mô-đun postmarkup
* Mô-đun unidecode
* Mô-đun zope.interface

Các mô-đun này có thể được cài đặt bằng **setuptools** (**easy_install**) hoặc **pip**.

Cài đặt
=======

Mã nguồn của VF có thể được tải về từ địa chỉ http://bitbucket.org/vithon/vithon-forum.

Giải nén mã nguồn vào một thư mục nào đấy, giả sử như **/opt/vithon-forum**.

Dựa vào nội dung của tập tin **config.py** để tạo tập tin **siteconfig.py** mới, với các dòng lệnh gán giá trị phù hợp cho biến toàn cục. Các biến quan trọng cần thay đổi là **COOKIE_SECRET**, **DATABASE_URL**, **STATIC_CONTENT_PATH**, **SESSION_STORAGE_PATH**.

Tập tin **siteconfig.py** có thể có dạng sau:

.. code-block:: python

  BOARD_NAME = 'Acme Hideout'
  COOKIE_SECRET = 'y@h0O'
  DATABASE_URL = 'sqlite:////opt/vithon-forum/db.db'
  STATIC_CONTENT_PATH = '/opt/vithon-forum/static'
  SESSION_STORAGE_PATH = '/tmp/session'
  SESSION_COOKIE_NAME = 'sid'
  ENTRIES_PER_PAGE = 10
  ORGANIZATION = 'Acme Corp'

Sau đó, chúng ta cần phải khởi tạo cơ sở dữ liệu bằng lệnh **initdb**.

.. code-block:: bash

  $ cd /opt/vithon-forum
  $ python forum.py initdb

Việc cài đặt đã hoàn tất.

Vận hành
========

Mặc dù cơ sở dữ liệu đã được khởi tạo khi cài đặt nhưng chúng ta chưa có dữ liệu nào trong cơ sở dữ liệu cả. Do đó, trước khi chạy VF, chúng ta sẽ cần phải tạo một số diễn đàn. Chúng ta sẽ sử dụng lệnh **shell** để thực hiện việc này.

.. code-block:: bash

  $ cd /opt/vithon-forum
  $ python forum.py shell

Lệnh **shell** sẽ mở một phiên làm việc tương tác cho phép chúng ta sử dụng Python để tác động đến chương trình VF. Chúng ta sẽ nhập vào đoạn mã Python như sau.

.. code-block:: python

  from model import *
  f1 = Forum(u'Forum 1', u'Dien giai cho Forum 1')
  f11 = Forum(u'Forum 1.1', u'Dien giai cho Forum 1.1', f1)
  f12 = Forum(u'Forum 1.2', u'Dien giai cho Forum 1.2', f1)
  f2 = Forum(u'Forum 2', u'Dien giai cho Forum 2')
  f21 = Forum(u'Forum 2.1', u'Dien giai cho Forum 2.1', f2)
  f22 = Forum(u'Forum 2.2', u'Dien giai cho Forum 2.2', f2)
  session.add(f1)
  session.add(f2)
  session.commit()

Các câu lệnh trên tạo ra cấu trúc diễn đàn như sau::

  Forum 1
    |
    +-- Forum 1.1
    |
    +-- Forum 1.2

  Forum 2
    |
    +-- Forum 2.1
    |
    +-- Forum 2.2

Vì VF không có các tính năng quản lý qua web nên lệnh **shell** cũng chính là môi trường quản lý chính.

Cuối cùng, để chạy VF, chúng ta sẽ dùng lệnh **runserver** tương tự như sau:

.. code-block:: bash

  $ cd /opt/vithon-forum
  $ python forum.py runserver -h 127.0.0.1 -p 8080

Khi này, nếu ta mở trình duyệt lên và đi đến trang http://127.0.0.1:8080 thì chúng ta sẽ thấy diễn đàn đã hoạt động.

Triển khai theo mô hình CGI
===========================

Chúng ta cũng có thể triển khai VF theo mô hình CGI.

Đầu tiên chúng ta cần chép tập tin **forum.cgi** vào thư mục chứa các tập tin CGI (ví dụ như **/var/www/cgi-bin**).

Tiếp đến chúng ta cần sửa các lệnh thiết lập đường dẫn trong tập tin này cho phù hợp.

.. code-block:: python

  # Set the path to store python package cache
  os.environ['PYTHON_EGG_CACHE'] = '/tmp/.egg'
  # Set the path to vithon forum here
  sys.path.insert(0, '/opt/vithon-forum')

Nếu như tập tin **siteconfig.py** nằm ở thư mục khác (ví dụ như trong thư mục **/var/www/cgi-bin**), thì chúng ta cũng sẽ cần thêm vào các dòng bên dưới. Nếu **siteconfig.py** nằm chung chỗ với các tập tin khác của VF thì chúng ta không cần các dòng lệnh này.

.. code-block:: python

  # Set the path to siteconfig.py here
  sys.path.insert(0, '/var/www/cgi-bin')

Và cuối cùng chúng ta chỉ cần cấu hình máy chủ web để sử dụng tập tin **forum.cgi** như là một ứng dụng CGI. Thông tin về vấn đề này được nói rõ hơn trong tài liệu đi kèm với máy chủ web.

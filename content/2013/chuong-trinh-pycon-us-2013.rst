**************************
Chương trình PyCon US 2013
**************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2013-01-18 16:04:00
:tags: pycon


Chương trình hội thảo PyCon năm nay đã được công bố vào bốn ngày trước.

https://us.pycon.org/2013/schedule/

Sau nhiều tuần đánh giá các bài tham luận, chương trình của hội thảo cuối cùng đã được thống nhất. Năm nay số lượng người tham dự tăng đột ngột và 1000 vé bán trước (early bird) đã không còn nữa. Tổng lượng vé bán ra được giới hạn ở mức 2500 vé. Nếu bạn muốn tham dự hội thảo, bạn cần phải `mua vé <https://us.pycon.org/2013/registration/>`_ **ngay bây giờ**. Hội thảo bắt đầu **từ ngày 13 đến ngày 21 tháng 03**.

So với năm ngoái, hội thảo năm nay có thêm một chuyên mục thứ sáu, nâng tổng số bài tham luận lên 114 bài. Các bài điểm của hội thảo sẽ được trình bày bởi những nhân vật tên tuổi như Eben Upton, Jessica McKellar, Raymond Hettinger, và Guido van Rossum.

PS: Nếu không có điều kiện tham dự hội thảo, các bạn cũng có thể xem phim quay lại tại http://pyvideo.org.
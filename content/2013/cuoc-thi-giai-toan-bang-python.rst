******************************
Cuộc thi giải toán bằng Python
******************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2013-03-31 02:18:00
:tags: maths, python, contest


Trong lần thi trước (http://www.vithon.org/2010/05/24/phat-d%E1%BB%99ng-cu%E1%BB%99c-thi-gi%E1%BA%A3i-toan-b%E1%BA%B1ng-python), chúng ta đánh giá chương trình dựa trên số lượng cặp N, K tìm được.

Tiêu chí của lần thi này là in ra cặp số N, K **lớn nhất** có thể tìm được trong 30 giây. Cặp số cuối cùng được in ra trong 30 giây sẽ được xem là cặp số lớn nhất và dùng để đánh giá các chương trình với nhau.

Giải thưởng của kỳ thi lần này sẽ là một áo thun do bạn Nguyễn Thành Nam đã tặng nhóm PCNV trong lần đi PyCon vừa qua. Các bạn có thể xem qua các áo giải thưởng (trừ EdX cỡ L, các áo khác cỡ M, chỉ được chọn 1) ở URL sau:

https://drive.google.com/folderview?id=0B5X03CmpgiFeNzNJN09qZnd2Sk0&usp=sharing

Các bài tham dự gửi bằng thư điện tử về cho admin@ hạn chót trong **ngày 14 tháng 04 năm 2013**. Nhằm khuyến khích việc sử dụng Python 3, các bài tham gia kỳ thi này sẽ được **chấm trên Python 3.3**. Mòng các bạn chú ý yêu cầu này.

Quyết định của người chấm là cuối cùng. Xin miễn nhận khiếu nại.
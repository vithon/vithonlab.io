****************************************
Phát động cuộc thi Giải Toán Bằng Python
****************************************


:author: Phan Đắc Anh Huy
:category: Tin tức
:date: 2010-05-24 07:32:00
:tags: maths, python, contest


Sau thành công của 2 lần thi trước, nhóm PCNV tiếp tục phát động cuộc thi Giải Toán Bằng Python để chào mừng ngày quốc tề thiếu nhi đang đến rất gần. Đề thi rất đơn giản:

  Hãy dùng ngôn ngữ lập trình Python để tính toán và xuất ra các cặp số n và k thỏa mãn::

    1 + 2 + ... + n = n + 1 + n + 2 + ... + n + k

Input:
  Không có.

Output:
  Standard Output.

Output Format:
  In trên nhiều dòng, mỗi dòng là một cặp số n, k cách nhau bằng một hoặc nhiều khoảng trắng.

Yêu cầu:
  - Chỉ được dùng các module built-in.
  - Không được in ra các giá trị n, k đã tính toán sẵn trước khi chương trình thực thi dưới bất kỳ hình thức nào.
  - Chương trình sẽ được người chấm thực thi trong vòng 30 giây. Hết thời gian này, chương trình sẽ bị ngắt.
  - Chương trình nào in ra được nhiều cặp số n, k nhất sẽ chiến thắng.
  - Chương trình sẽ bị loại nếu in ra bất cứ cặp số n, k nào không chính xác.

Thời hạn dự thi: Từ ngày hôm nay (24/05/2010) đến hết ngày **31/05/2010**.

Bài dự thi xin gửi về địa chỉ email **admin+frontpage@vithon.org** (xin vui lòng bỏ +frontpage trước khi gửi), bạn có thể gửi nhiều lần trong trường hợp cải thiện được kết quả của mình. Chương trình được gửi cuối cùng sẽ được nhóm PCNV sử dụng để chấm điểm chung cuộc.

Trong quá trình làm bài, nếu có nhu cầu trao đổi, đặt câu hỏi, hay chỉ đơn giản là muốn trò chuyện với những người bạn rất dễ thương trong nhóm PCNV, các bạn có thể tham gia phòng tán gẫu ``#vithon`` ở máy chủ ``irc.freenode.net``.

Đã là cuộc thi thì không thể thiếu giải thưởng, phần thưởng cho người thắng cuộc là 100.000 VND và một buổi trò chuyện thân mật với admin nhóm PCNV.

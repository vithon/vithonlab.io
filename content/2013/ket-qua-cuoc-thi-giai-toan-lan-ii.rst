*********************************
Kết quả cuộc thi giải toán lần II
*********************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2013-04-18 16:29:00
:tags: python, optimize, contest


Cảm ơn sự hưởng ứng nhiệt tình của các bạn với cuộc thi giải toán lần II của PCNV.

Lần này nhóm nhận được sự sự tham gia của năm bạn Phạm Ngọc Quang Nam, Nguyễn Văn Nam, Vũ Khuê, Hoàng Quốc Thịnh, và Đậu Duy Khánh.

Bài của bạn Thịnh::

  09:03:48 ~/tmp/pell2$ python3.3 thinhhq.py | python timer.py > thinhhq.txt
  09:04:22 ~/tmp/pell2$ python checker.py < thinhhq.txt
  Max 448985
  09:04:30 ~/tmp/pell2$ python3.3 thinhhq.py | python timer.py > thinhhq.txt2
  09:05:04 ~/tmp/pell2$ python checker.py < thinhhq.txt2
  Max 449905
  09:05:08 ~/tmp/pell2$ python3.3 thinhhq.py | python timer.py > thinhhq.txt3
  09:05:39 ~/tmp/pell2$ python checker.py < thinhhq.txt3
  Max 449421

Bài của bạn Văn Nam::

  09:05:46 ~/tmp/pell2$ python3.3 namnv.py | python timer.py > namnv.txt
  09:07:12 ~/tmp/pell2$ python checker.py < namnv.txt
  Max 1394855
  09:07:28 ~/tmp/pell2$ python3.3 namnv.py | python timer.py > namnv.txt2
  09:07:47 ~/tmp/pell2$ python checker.py < namnv.txt2
  Max 1394855
  09:08:00 ~/tmp/pell2$ python3.3 namnv.py | python timer.py > namnv.txt3
  09:08:19 ~/tmp/pell2$ python checker.py < namnv.txt3
  Max 1394855

Bài của bạn Khanh::

  09:12:49 ~/tmp/pell2$ python3.3 khanhdd.py | python timer.py > khanhdd.txt
  Traceback (most recent call last):
    File "khanhdd.py", line 13, in <module>
      print (a1,' ',b1)
  BrokenPipeError: [Errno 32] Broken pipe
  09:13:27 ~/tmp/pell2$ python checker.py < khanhdd.txt
  ERROR:root:Format
  Traceback (most recent call last):
    File "checker.py", line 16, in <module>
      n, k = map(long, line.split())
  ValueError: too many values to unpack

Bài của bạn Khuê::

  09:14:37 ~/tmp/pell2$ python3.3 khuev.py | python timer.py > khuev.txt
    File "khuev.py", line 35
      print "n: %d - k: %d" % (-b / 2, k)
                        ^
  SyntaxError: invalid syntax

Bài của bạn Quang Nam::

  09:15:19 ~/tmp/pell2$ python3.3 nampnq.py | python timer.py > nampnq.txt
    File "nampnq.py", line 5
      print "n=%d, k=%d" % (n,k)
                     ^
  SyntaxError: invalid syntax

Mã nguồn và bản lưu kết quả của cuộc thi có thể được tải về từ  https://docs.google.com/file/d/0B5X03CmpgiFeMkNubXlGNFlCMjQ/edit?usp=sharing.

Như vậy, bạn Văn Nam đã thắng giải kỳ này. Xin chúc mừng bạn Văn Nam.

Bạn Văn Nam gửi thư cho admin@ cho biết mẫu áo bạn chọn, và thông tin bưu điện để nhóm PCNV có thể gửi áo cho bạn.
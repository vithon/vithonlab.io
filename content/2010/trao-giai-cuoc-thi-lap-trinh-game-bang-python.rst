*********************************************
Trao giải cuộc thi lập trình game bằng Python
*********************************************


:author: Nguyễn Thành Nam
:category: Tin tức
:date: 2010-02-05 03:56:00
:tags: game, prize, contest


Ngày 04 tháng 02 năm 2009, nhóm PCNV đã trao giải cuộc thi lập trình game với ngôn ngữ Python cho bạn **Hoàng Quốc Thịnh**.

Cuộc thi lập trình game với ngôn ngữ Python là một trong những hoạt động của nhóm PCNV nhằm lôi cuốn sự tham gia tìm hiểu và khuyến khích sáng tạo trong việc sử dụng ngôn ngữ Python.

Ngôn ngữ Python là một ngôn ngữ mạnh, đơn giản, và tổng quát. Ngoài việc được sử dụng trong các tổ chức lớn như Google, và NASA, Python còn được dùng để viết nên những trò chơi như EVE-Online với cả triệu người chơi, hay **Ruồi Yêu Cứt** với giải thưởng lập trình game bằng Python đầu tiên.

Xin chúc mừng bạn Hoàng Quốc Thịnh.

Hình bên dưới là bạn Nguyễn Thành Nam đại diện nhóm PCNV đang trao giải cho bạn Hoàng Quốc Thịnh trong bữa ăn thân mật. Hình chụp bởi nhiếp ảnh gia Thành Nguyễn (Malaysia) và sửa đôi chút bởi nhiếp ảnh gia Nguyễn Duy Thọ.

.. image:: /static/game_programming_contest_prize.png
******************************************
Hội thảo PyCon Châu Á Thái Bình Dương 2011
******************************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2011-02-14 11:07:00
:tags: 2011, pycon, apac


.. image:: http://pycon.sit.rp.sg/logo.png

Hội thảo PyCon APAC 2011 đã mở cửa kêu gọi bài đăng ký.

Để biết thêm chi tiết và gửi bài tham gia, hoặc đăng ký dự, các bạn hãy vào trang http://apac.pycon.org/.

PCNV là một trong các nhóm hỗ trợ hội thảo.

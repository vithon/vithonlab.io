****************************************************
Hội thảo PyCon khu vực Châu Á - Thái Bình Dương 2010
****************************************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2009-10-27 20:15:00
:tags: pycon, apac, community


.. image:: /static/pycon.apac.2010.jpg

PyCon APAC 2010
===============

Hội thảo `PyCon Asia Pacific <http://apac.pycon.org/>`_ sẽ được tổ chức tại Singapore vào ngày 09 đến 11 tháng 06 năm 2010. Ngày 09 sẽ dành cho các bài hướng dẫn và ngày 10, 11 sẽ dành cho các bài thảo luận.

Ban tổ chức kêu gọi sự tham gia của các cá nhân, tập thể trong các bài hướng dẫn lẫn các bài tham luận hội thảo về tất cả các khía cạnh của Python -- từ lập trình không chuyên đến lập trình chuyên nghiệp, về các ứng dụng hoặc khung phần mềm, và bạn sử dụng Python như thế nào trong công việc.

Các diễn giả lần đầu, đặc biệt là những diễn giả từ khu vực châu Á, được khuyến khích gửi bài cho ban tổ chức.

Thông tin thêm về hội thảo PyCon Châu Á - Thái Bình Dương có thể được tìm thấy tại http://apac.pycon.org/
****************************
Lập trình web với Python (6)
****************************


:author: Nguyễn Thành Nam
:category: Bài viết
:date: 2011-07-01 10:05:00
:tags: python, web, tutorial, fcgi


Phần trước chúng ta đã xem xét mô hình hoạt động của một ứng dụng FCGI. Trong phần này chúng ta sẽ viết thử một chương trình FCGI như đã đề cập.

Trước tiên, chúng ta sẽ cần cài đặt thư viện **flup** của tác giả Allan Saddi. Làm việc này khá đơn giản, chúng ta chỉ việc tải tập tin **ez_setup.py** từ địa chỉ http://peak.telecommunity.com/dist/ez_setup.py về một nơi nào đó trên máy, ví dụ như **C:\Python26\Scripts**. Sau đó chúng ta sẽ gọi lệnh ở màn hình DOS như sau::

  C:\>c:\Python26\scripts\ez_setup.py flup

Gói **flup** sẽ được cài đặt trong **C:\Python26\lib\site-packages**.

Ở bước 2, chúng ta sẽ tải về tập tin http://www.fastcgi.com/dist/mod_fastcgi-2.4.6-AP22.dll, đổi tên nó thành **mod_fastcgi.dll** và đặt nó trong **C:\Program Files\Apache Software Foundation\Apache2.2\modules**.

Kế tiếp bước 3, chúng ta sẽ cần cấu hình **Apache** thành một **FCGI client**. Chúng ta cần sửa tập tin **httpd.conf** để thêm vào các dòng sau ở dưới cùng::

  LoadModule fastcgi_module modules/mod_fastcgi.dll
  Alias "/fcgi-bin/hello.py" "C:/Program Files/Apache Software Foundation/Apache2.2/fcgi-bin/hello.py"
  <Directory "C:/Program Files/Apache Software Foundation/Apache2.2/fcgi-bin/">
    Order allow,deny
    Allow from all
  </Directory>
  FastCgiExternalServer "C:/Program Files/Apache Software Foundation/Apache2.2/fcgi-bin/hello.py" -host localhost:8888

Dòng **LoadModule** bảo **Apache** nạp mô-đun **mod_fastcgi** mà chúng ta vừa làm ở bước 2. Dòng **Alias** cho **Apache** biết rằng mọi yêu cầu đến đường dẫn (URI) **/fcgi-bin/hello.py** thật chất là đến tập tin **C:/Program Files/Apache Software Foundation/Apache2.2/fcgi-bin/hello.py**. Chúng ta cần dòng này bởi vì **hello.py** sẽ được tạo ở ngoài **DocumentRoot**. Cũng chính vì thư mục **fcgi-bin** không nằm trong **DocumentRoot** nên chúng ta cũng cần lệnh **Directory** để cho phép **Apache** phục vụ những yêu cầu truy cập vào thư mục đó. Và cuối cùng lệnh **FastCgiExternalServer** bảo **Apache** rằng tập tin **hello.py** thật chất là một ứng dụng FCGI đã được chạy sẵn tại địa chỉ **localhost** ở cổng **8888**. Chính câu lệnh này sẽ chuyển **Apache** thành một **FCGI client**, chuyển tiếp yêu cầu từ trình duyệt đến **FCGI server** do chúng ta phát triển. Vì chúng ta đang cài đặt trên Windows nên chúng ta chỉ có một lựa chọn là sử dụng FCGI theo chế độ trao đổi qua socket, do đó chúng ta phải dùng **FastCgiExternalServer**.

Bước 4 là tạo tập tin **hello.py** trong thư mục **C:/Program Files/Apache Software Foundation/Apache2.2/fcgi-bin**. Nếu chưa có thư mục này, bạn đọc hãy tự tạo ra nó. Tập tin **hello.py** sẽ có nội dung như sau:

.. code-block:: python

  #!C:\Python26\python.exe
  from flup.server import fcgi

  count = 0

  class HelloApp(fcgi.WSGIServer):

	def handler(self, req):
		global count
		count += 1
		req.stdout.write('Content-Type: text/plain\n')
		req.stdout.write('\n')
		req.stdout.write('Hello World %d' % count)

  if __name__ == "__main__":
	HelloApp(application=None, bindAddress=("localhost", 8888)).run()

Trước hết, chúng ta nhập mô-đun **flup.server.fcgi**. Kế tiếp chúng ta khai báo một biến đếm tên **count** với giá trị khởi tạo là **0**. Sau đó chúng ta khai báo ứng dụng của chúng ta bằng cách định nghĩa lớp **HelloApp** kế thừa lớp **WSGIServer** trong mô-đun **flup.server.fcgi**. **HelloApp** sẽ định nghĩa lại hàm **handler** để xử lý các yêu cầu nhận được.

Hàm **handler** nhận vào một tham số đó là đối tượng **req** chứa thông tin về các biến môi trường, các dòng dữ liệu chuẩn như **stdin**, **stdout**. Chúng ta sẽ cộng một vào biến đếm toàn cục, và xuất giá trị này ra lại cho trình duyệt. Như một ứng dụng CGI (FCGI) thông thường, trước hết chúng ta cần phải xác định kiểu dữ liệu trả về thông qua **Content-Type**, theo sau bởi một dòng trống đánh dấu sự chấm dứt các đầu đề (header) và bắt đầu của nội dung, và cuối cùng là nội dung.

Hai dòng lệnh cuối chỉ đơn giản là chạy ứng dụng FCGI này ở địa chỉ **localhost** và cổng **8888**.

Để kiểm tra, chúng ta sẽ cần thực hiện ba việc: chạy ứng dụng **hello.py**, chạy **Apache**, và dùng trình duyệt xem trang **http://localhost/fcgi-bin/hello.py**. Để chạy ứng dụng **hello.py** chúng ta chỉ cần nhập lệnh sau ở màn hình DOS::

  c:\Program Files\Apache Software Foundation\Apache2.2\fcgi-bin>python hello.py

Khi dùng trình duyệt vào xem trang web như trên thì chúng ta sẽ nhận được hình sau:

.. image:: /static/web-programming/fcgi/hello1.png

Nếu làm tươi trình duyệt thì chúng ta sẽ thấy giá trị đếm tăng lên:

.. image:: /static/web-programming/fcgi/hello2.png

Như vậy chứng tỏ rằng các yêu cầu đều được phục vụ bởi một tiến trình duy nhất. Đây chính là lợi điểm của FCGI so với CGI.

Trong bài sau, chúng ta sẽ xem xét cách phát triển ứng dụng web với WSGI.

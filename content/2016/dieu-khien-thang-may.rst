********************
Điều khiển thang máy
********************


:author: Nhóm PCNV
:category: Tin tức
:date: 2016-06-23 13:58:00
:tags: t-shirt, python, codelift


**Cập nhật:** Do số lượng bài tham gia vẫn còn ít (dẫn đến cuộc thi đua không còn thú vị nữa) nên ban tổ chức dời lại hạn nộp bài đến **hết ngày 21 tháng 07**.

Nhân việc PyCon vừa qua, nhóm PCNV hân hạnh tổ chức cuộc thi **Điều khiển thang máy!**

Tương tự như cuộc thi `Bắn suồng <http://www.vithon.org/2010/11/26/tranh-tai-bắn-suồng>`_, cuộc thi **Điều khiển thang máy** cũng là một cuộc thi lập trình **Python** và có một số điều lệ sau:

#. Trò chơi được giới thiệu tại http://codelift.org. Người tham dự có thể thử mã của mình tại trang mạng đó.
#. Người tham gia phải nộp mã Python (tập tin **.py**) tới **admin+codelift@vithon.org**, và đồng ý với việc nhóm PCNV có toàn quyền sử dụng mã đó cho mọi mục đích.
#. Người tham gia cũng phải gửi thêm một bài viết (dưới dạng **.txt**) giới thiệu trò chơi, và cách giải của mình đến cùng địa chỉ thư trên.
#. Hạn nhận bài là **ngày 14 tháng 07 năm 2016.**
#. Bài thi sẽ được đọc qua trước, và sẽ được chạy trên cùng hệ thống codelift.org.
#. Bài thi với số điểm trung bình cao nhất trong **3 tòa nhà** được chọn khi chấm sẽ là bài thắng giải.
#. **Ba** phần thưởng dành cho ba bài thi với số điểm trung bình cao nhất là ba cái áo thun và một số miếng dán mà bạn Nam đã gửi cho nhóm từ PyCon. Người hạng nhất sẽ được quyền chọn mẫu áo trước, rồi đến hạng nhì, và hạng ba. Mẫu áo đã được công bố trong `thư mục chia sẻ <https://drive.google.com/folderview?id=0B4aYVYJVZM_zT2tzZGdTNTUwQWc&usp=sharing>`_ ở bài trước.
#. Quyết định của ban tổ chức là cuối cùng.

Mọi thắc mắc và thảo luận xin hãy dùng `diễn đàn <forum.vithon.org>`_.

Chúc may mắn và nhiều niềm vui!
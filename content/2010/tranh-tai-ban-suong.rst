*******************
Tranh tài Bắn Suồng
*******************


:author: Nhóm PCNV
:category: Tin tức
:date: 2010-11-26 03:23:00
:tags: battleship, game, contest


Cập nhật: Phiên bản ``battleship.zip`` cũ có lỗi. Các bạn tải về bản mới hoặc sửa dòng ``player2_mod = __import__(opts.player_1)`` thành ``player2_mod = __import__(opts.player_2)``. Xin cáo lỗi vì sự phiền toái này.

Nhóm PCNV hân hạnh tổ chức một cuộc thi mới: **Tranh tài Bắn Suồng!**

Bắn Suồng (xin lỗi vì sai chính tả, nhưng để giữ chữ viết tắt BS nên tôi đành phải đặt tên trò chơi là vậy) không phải là một trò chơi thông thường. Để chơi trò chơi này, các bạn phải viết mã bằng ngôn ngữ... (cho xin 500 đồng trống và kèn cho hào hứng tí nào) Python để điều khiển các đối tượng trong trò chơi.

Các bạn có thể tải trò chơi về tại địa chỉ http://www.vithon.org/static/battleship.zip.

Cuộc thi Bắn Suồng được tổ chức với thể lệ như sau:

#. Mô-đun (các tập tin **.py**) tham gia phải gửi về cho admin+thư-rác@vithon.org (bỏ phần + thư rác) trước ngày 12 tháng 12 năm 2010.
#. Các bài thi này sẽ được kiểm tra trước khi được chấp nhận cho thi.
#. Các bài tham dự sẽ đấu vòng tròn tính điểm. Thắng sẽ được 3 điểm, hòa được 1, và thua được 0 điểm.
#. Các trận đấu sẽ được truyền hình (qua web) trực tiếp vào ngày 12 tháng 12.
#. Vùng biển sẽ có kích thước ngẫu nhiên với độ rộng và dài tối thiểu là 10, và tối đa là 15.
#. Một giải nhất sẽ nhận được 01 chiếc áo Vithon miễn phí, và có thể mua thêm 01 chiếc áo khác với giá ưu đãi **100.000**!
#. Quyết định của ban tổ chức là cuối cùng. Xin được phép không nhận khiếu nại.

Để trao đổi về cuộc thi này, cũng như các vấn đề liên quan đến lập trình Python, các bạn có thể sử dụng `diễn đàn <forum.vithon.org>`_ của nhóm hoặc liên hệ qua IRC.

Nhóm PCNV rất mong nhận được sự tham gia nhiệt tình của các bạn.
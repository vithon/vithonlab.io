*********************************************
Cảnh báo: Cẩn thận khi sử dụng pip trong mạng
*********************************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2013-02-03 19:37:00
:tags: pip, ssl, security, certificate


Pip (http://pypi.python.org/pypi/pip), cũng như một số các công cụ sử dụng mô-đun có sẵn ``urllib``, ``urllib2``, ``httplib``, có thể bị tấn công kẻ ở giữa (man-in-the-middle) và làm thay đổi nội dung của gói tin nhận được.

Lý do là khi truy cập vào một địa chỉ HTTPS hoặc các giao thức SSL/TLS, các mô-đun có sẵn này không kiểm tra chứng từ (certificate) của máy chủ để xác nhận máy chủ đó đúng là máy chủ chương trình cần truy cập. Điều này cho phép một kẻ thứ ba giả dạng máy chủ và thay đổi nội dung gói tin yêu cầu cũng như trả lời.

Do đó, người sử dụng cần cẩn trọng khi sử dụng các công cụ như ``pip`` trong một mạng không an toàn.

Thông tin thêm có thể được tham khảo tại:

http://www.reddit.com/r/Python/comments/17rfh7/warning_dont_use_pip_in_an_untrusted_network_a/

Kể từ Python 3.2, các mô-đun có sẵn liên quan đến SSL/TLS đã có thêm chức năng kiểm tra chứng từ nhưng chức năng này phải được tự kích hoạt.

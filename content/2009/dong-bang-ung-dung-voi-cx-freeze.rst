********************************
Đóng băng ứng dụng với cx_Freeze
********************************


:author: Nguyễn Thành Nam
:category: Bài viết
:date: 2009-12-01 05:54:00
:tags: exe, freeze


Python là ngôn ngữ thông dịch nên để chạy các ứng dụng viết bằng Python, chúng ta cần phải cài bộ thông dịch. Thao tác này hơi luộm thuộm đối với người dùng cuối, nên thông thường chúng ta hay tự hỏi liệu có cách nào đó để chuyển ứng dụng của chúng ta thành một tập tin .EXE như các ứng dụng Windows khác không.

Rất may mắn là chúng ta có thể đạt được mục đích này với chương trình `cx_Freeze <http://cx-freeze.sourceforge.net>`_.

Chúng ta sẽ bắt đầu với một ví dụ đơn giản. Hãy tạo một tập tin ``helloworld.py`` với nội dung sau:

.. code-block:: python

  # helloworld.py
  print "Hello World!"

Khi chạy chương trình bé nhỏ này (giả sử nó đang nằm trong thư mục ``c:\tmp``, chúng ta nhận kết quả như sau::

  c:\tmp>c:\Python26\python helloworld.py
  Hello World!

Bây giờ chúng ta sẽ cài đặt cx_Freeze. Đối với ví dụ trên, chúng ta sẽ phải chọn bản cài đặt cho Windows, và cho phiên bản Python 2.6. Sau khi cài đặt cx_Freeze thì tập tin thực thi sẽ được đặt trong ``c:\python26\scripts\cxfreeze``.

Để đóng băng chương trình ``helloworld.py``, chúng ta thực hiện lệnh ``cxfreeze helloworld.py``::

  c:\tmp>c:\Python26\scripts\cxfreeze helloworld.py
  coyping c:\python26\lib\site-packages\cx_Freeze\bases\Console.exe -> c:\tmp\dist\helloworld.exe
  coyping C:\Windows\system32\python26.dll -> c:\tmp\dist\python26.dll
  writing zip file c:\tmp\dist\helloworld.exe

    Name                      File
    ----                      ----
  m StringIO
  m UserDict
  m __builtin__
  .
  .
  .
  m warnings
  m zipimport
  m zlib

  coyping c:\python26\DLLs\bz2.pyd -> c:\tmp\dist\bz2.pyd
  coyping c:\python26\DLLs\select.pyd -> c:\tmp\dist\select.pyd
  coyping c:\python26\DLLs\unicodedata.pyd -> c:\tmp\dist\unicodedata.pyd

Kết quả của lệnh này là ``helloworld.py`` được đóng băng trong thư mục ``dist``::

  c:\tmp>dir /s dist
   Volume in drive C is OS
   Volume Serial Number is B07D-8588

   Directory of c:\tmp\dist

  12/01/2009  12:42 PM    <DIR>          .
  12/01/2009  12:42 PM    <DIR>          ..
  12/01/2009  12:42 PM            71,168 bz2.pyd
  12/01/2009  12:42 PM         1,423,792 helloworld.exe
  12/01/2009  12:42 PM         2,149,888 python26.dll
  12/01/2009  12:42 PM            11,776 select.pyd
  12/01/2009  12:42 PM           585,728 unicodedata.pyd
                 5 File(s)      4,242,352 bytes

       Total Files Listed:
                 5 File(s)      4,242,352 bytes
                 2 Dir(s)  118,923,808,768 bytes free

Và bây giờ chúng ta có thể đóng gói, hoặc nén các tập tin trong thư mục này để chuyển đến người dùng cuối. Tập tin thực thi chính của chúng ta chính là ``helloworld.exe``::

  c:\tmp>cd dist

  c:\tmp\dist>helloworld.exe
  Hello World!

cx_Freeze còn nhận một số tham số dòng lệnh khác. Bạn có thể tham khảo thêm bằng cách chạy ``cxfreeze --help``.

Ngoài cx_Freeze còn có một số công cụ khác có chức năng tương tự ví dụ như `py2exe <http://www.py2exe.org/>`_.
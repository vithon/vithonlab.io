***********
..and..or..
***********


:author: Phạm Thị Minh Hoài
:category: Bài viết
:date: 2010-01-02 16:25:00
:tags: None, ngôn ngữ


Trong bài viết trước về **Phần tử không** tôi có nói về việc cần phải cẩn thận khi dùng phép toán có tương tác giữa phần tử không và **None**. Trong bài này tôi trình bày một ví dụ nhỏ minh họa rõ hơn một vấn đề mà nhiều bạn mới học có thể gặp phải. Đó là phép toán rút gọn **..and..or..** khi có sự tham gia của **None**. 

Cấu trúc **if.. else** và phép gán giá trị theo điều kiện trong python:

.. code-block:: python

  if condition:
      x = somevalue
  else:
      x = othervalue

Có thể được viết trên một dòng:

.. code-block:: python

  x = condition and somevalue or othervalue

Ví dụ:

.. code-block:: pycon

  >>> x = 1+1*2 == 3 and "OK" or "DUREX"
  >>> x
  'OK'

Thực tế có nhiều bạn thích cách viết này. Vì nó gọn hơn và *mang tính python* hơn. Tuy nhiên hãy cẩn thận khi sử dụng cấu trúc này. Chẳng hạn trong cấu trúc:

.. code-block:: python

  x = condition and func1() or func2()

Hàm **func1()** và **func2()** có thể có giá trị **None**, khi đó kết quả kỳ vọng có thể không như bạn mong muốn. Xét ví dụ:

.. code-block:: python

  def func1():
      return None

  def func2():
      return "some value"

  x = 1 + 1 == 2 and func1() or func2()
  print x

Kết quả::

  some value

Biểu thức logic là **True** song vì **func1() == None** nên **x = True and None or func2()** = **"some value"**

Vì vậy hãy cẩn thận khi dùng cấu trúc **..and..or..** trừ khi bạn biết rõ các giá trị tham gia biểu thức này luôn luôn khác **None**.

Từ phiên bản 2.5 trở đi Python hỗ trợ cấu trúc:

.. code-block:: python

  x = TrueValue if Condition else FalseValue

Ví dụ:

.. code-block:: python

  x = "OK" if 1+1 == 2 else "DUREX"
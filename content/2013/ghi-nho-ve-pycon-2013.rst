*********************
Ghi nhớ về PyCon 2013
*********************


:author: Nhóm PCNV
:category: Tin tức
:date: 2013-03-18 06:01:00
:tags: pycon


Hội thảo PyCon 2013 vừa kết thúc vào Chủ Nhật, ngày 18 tháng 03 năm 2013 vừa rồi tại Trung tâm Hội nghị Santa Clara, bang California, Mỹ.

Hội thảo lần này diễn ra vào các ngày thứ sáu, thứ bảy, và Chủ Nhật với khoảng 100 bài tham luận và 2500 người tham dự đến từ tất cả các châu lục trên trái đất. Trong đó, khoảng 20% là nữ giới!

Nhóm PCNV có một thành viên tham gia hội thảo lần này, và họ có một số nhận xét chính như sau:

1. Hội thảo có nhiều chuyên mục phù hợp với mọi lứa tuổi, mọi ngành nghề, mọi giới tính. Có cả những em thiếu niên khoảng 10 đến 12 tuổi tham gia hội thảo. Một số các em đã biết Python còn đứng lớp chỉ dạy Python cho các em khác. Thậm chí có một số em đã trình bày sản phẩm của chính mình trong phân mục tự giới thiệu (poster session), cùng với gần trăm sản phẩm khác từ các công ty lớn cũng như các chuyên gia trong ngành. Nữ giới chiếm gần 20% người tham gia hội thảo. Một số cá nhân trên 60 tuổi cũng thấy xuất hiện tại hội thảo. Thậm chí cả người chuyển đổi giới tính cũng có mặt! Có thể khẳng định chắc chắn rằng Python thu hút được sự quan tâm của tất cả mọi người.

2. Hội thảo năm nay có sự xuất hiện của nhiều nhóm nữ giới như CodeChix, WomanWhoCode, PyLadies, LadyCoders. Đây là một tín hiệu đáng mừng. So với các ngôn ngữ lập trình đa dụng khác, có lẽ Python là ngôn ngữ thu hút nữ giới nhiều nhất!

3. Hội thảo năm nay có một sự ngạc nhiên thú vị. Mỗi cá nhân tham dự hội thảo được tặng một thiết bị Rasberry Pi miễn phí! Điều này cho thấy Python có một vị trí quan trọng trong thị trường phần cứng nghiệp dư, kinh tế. Trong hội thảo cũng thiết lập một "Phòng nghiên cứu Rasberry Pi" để hướng dẫn cách cài đặt và sử dụng thiết bị đó.

4. Các bài tham luận chớp nhoáng (lightning talks) cho thấy Python có khả năng ứng dụng rộng rãi và đáng ngạc nhiên. Một số ví dụ điển hình như sử dụng Python để điều khiển hệ thống đèn LED nhấp nháy, sử dụng Python trên các hệ máy Atari, sử dụng Python để thông dịch và gỡ rối mã Lisp/Scheme.

5. Lập trình viên Python có khả năng Rap không thua kém các ca sĩ chuyên nghiệp! Đoạn phim bên dưới quay lại cảnh Larry Hastings, thành viên nòng cốt trong nhóm phát triển Python, hát bài rap tự chế trước phiên tham luận chớp nhoáng.

http://youtu.be/rKiySLUrYQ8

6. Bạn Nguyễn Thành Nam có mặt tại hội thảo đã chụp tấm hình tự sướng này.

https://docs.google.com/file/d/0B5X03CmpgiFeb3pfMTlIVlBCQ3M/edit?usp=sharing

Bạn Nam cũng đã ủng hộ một số món quà nhỏ cho nhóm PCNV dùng để khuyến khích các bạn đã có bài viết đóng góp cho nhóm. Nhóm PCNV sẽ gửi trực tiếp cho các bạn.
*******************************
PyCon 2016 ở thành phố Portland
*******************************


:author: Nguyễn Thành Nam
:category: Tin tức
:date: 2016-06-12 18:07:00
:tags: pycon


Hai tuần trước mình có tham dự hội thảo PyCon 2016 ở thành phố Portland, tiểu bang Oregon, vùng Tây Bắc nước Mỹ.

Cũng như mọi năm, PyCon vẫn là hội thảo tuyệt vời nhất từ trước đến nay. Tham dự hội thảo không chỉ có lập trình viên mà còn có nhiều nhà khoa học, giáo viên, sinh viên, và cả học sinh tiểu học nữa! Mình chưa từng thấy hội thảo khoa học kỹ thuật nào mà có thể quy tập được nhiều đối tượng tham gia như vậy.

Một điều tuyệt vời nữa là có rất nhiều các bạn nam, các bạn nữ, và cả các bạn chuyển giới tham gia PyCon. Điều này cho thấy Python không chỉ là một ngôn ngữ "có sẵn pin", mà có còn là một cộng đồng rất "bao gồm". Ai cũng được hoan nghênh, ai cũng được đối xử một cách tôn trọng, không có sự phân biệt đối xử.

Sau đây là một số hình ảnh mình ghi lại để chia sẻ cùng các bạn. Hy vọng một dịp nào đó sẽ gặp được các bạn ở trong các kỳ hội thảo trong tương lai:

`Bảng hiệu PyCon 2016 ở dọc đường đến trung tâm hội nghị Portland. <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zUU9DNkpxYWt5Smc>`_

`Em bé đứng nhìn tượng Martin Luther King, Jr. ở trước trung tâm hội nghị. <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zX3VIeEpKd3FpWWs>`_

`Hội trường chính, nơi diễn ra các bài tham luận chủ chốt. <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zUE13c2xUUk16eUE>`_

Các bạn có thể thấy con lắc biểu tượng của trung tâm hội nghị Portland trong hai hình sau:

`Khu đăng ký (hình 1). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zbnk2VUNOaUlJYm8>`_

`Khu đăng ký (hình 2). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zT1NhYmNRb2VXaDA>`_

`Khu vực phòng Oregon (hình 1). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zTVl3Z1lpSTRNUDQ>`_

`Khu vực phòng Oregon (hình 2). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zcE1aVGRnbHAwb1k>`_

`Khu vực phòng Oregon (hình 3). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zam1VeDduZEhidWc>`_

`Khu vực phòng Oregon (hình 4). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zUWRiUXlOamdkV0E>`_

`Khu vực phòng Portland (hình 1). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zZTZBOUtCenN4cGs>`_

`Khu vực phòng Portland (hình 2). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zbGpDZkpsRUh2bEk>`_

`Hình Guido bắt đầu bài tham luận chính. <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zemFaTWJYSVg0Z00>`_ Guido vẫn mặc chiếc áo bé gái như mọi năm. Không biết cái áo này sẽ còn theo Guido đến PyCon năm nào đây. Điều này cũng cho thấy Guido rất quan tâm đến sự đối xử bình đẳng, tôn trọng giới tính ở hội thảo PyCon.

`Bảng đăng ký không gian mở (open spaces) ở hội thảo. <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zSGZHeEx1VF9jSTQ>`_

`Ảnh trưng bày (poster session) (hình 1). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zT19tZ0k5dl9kLVE>`_

`Ảnh trưng bày (poster session) (hình 2). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zRUhseUdyVnhRbXM>`_

Hai hình ảnh của tác giả bài viết:

`Ảnh đẹp nhất hội thảo (hình 1). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zVjR0RVZncHoySVU>`_

`Ảnh đẹp nhất hội thảo (hình 2). <https://drive.google.com/uc?export=download&id=0B4aYVYJVZM_zWWxNZlZTMWg3NFU>`_

Toàn bộ các hình ảnh có thể được truy cập trong thư mục https://drive.google.com/folderview?id=0B4aYVYJVZM_zT2tzZGdTNTUwQWc&usp=sharing.
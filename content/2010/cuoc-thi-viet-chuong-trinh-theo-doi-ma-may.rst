******************************************
Cuộc thi viết chương trình theo dõi mã máy
******************************************


:author: Nguyễn Thành Nam
:category: Tin tức
:date: 2010-07-22 05:13:00
:tags: gdb, contest, tracer, ptrace


Một người bạn của chúng ta đã có nhã ý tài trợ cho một cuộc thi nhỏ.

Mục tiêu: Viết một công cụ bằng ngôn ngữ Python để thực hiện việc lưu lại những lệnh gọi hàm ở mức hợp ngữ trong khi thực thi của một chương trình thông thường khác.

Ví dụ: Với đoạn mã tương tự như sau:

.. code-block:: asm

  0x08048552 <main+110>:	mov    DWORD PTR [esp],eax
  0x08048555 <main+113>:	call   0x80483d8 <printf@plt>
  0x0804855a <main+118>:	mov    eax,0x0
  0x0804855f <main+123>:	leave  
  0x08048560 <main+124>:	ret 

thì chương trình sẽ in ra thông tin tương tự (có thể tùy ý chỉnh sửa cho đầy đủ hơn) như sau:

.. code-block:: asm

  * Before printf()
  --------------------------------------------------------------------------[regs]
    EAX: 0x08048646  EBX: 0x002B2FF4  ECX: 0x00000003  EDX: 0xBFFFEF9C  
    ESI: 0x00000000  EDI: 0x00000000  EBP: 0xBFFFF0A8  ESP: 0xBFFFEF80  

  * After printf()
  --------------------------------------------------------------------------[regs]
    EAX: 0x0000000A  EBX: 0x002B2FF4  ECX: 0xBFFFEF68  EDX: 0x002B4320  
    ESI: 0x00000000  EDI: 0x00000000  EBP: 0xBFFFF0A8  ESP: 0xBFFFEF80

Yêu cầu: Bắt buộc phải sử dụng ngôn ngữ Python cho chương trình chính. Có thể sử dụng thêm các công cụ phụ hoặc thư viện hỗ trợ như GDB, ptrace.

Hạn nộp bài: Ngày 26 tháng 07 năm 2010

Nộp bài (mã nguồn) về cho admin+frontpage@vithon.org (xin vui lòng bỏ +frontpage).

Giải thưởng: Một giải thưởng duy nhất bao gồm 100.000 đồng tiền mặt và một món quà từ Las Vegas.

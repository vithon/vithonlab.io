*************
Python và PHP
*************


:author: Nhóm PCNV
:category: Bài viết
:date: 2009-01-30 03:01:00
:tags: python, comparison, php


Bài viết này được tổng hợp từ nhiều nguồn khác nhau (bằng tiếng anh) cộng với kinh nghiệm viết PHP và Python của tôi.

I. Giống nhau
=============

* Là ngôn ngữ cao cấp, thông dịch và định kiểu động 
* Mã nguồn mở 
* Cộng đồng lập trình viên rộng lớn 
* Dễ học (so với Java, C hay Perl) 
* Dễ mở rộng (so với Java, C++) 
* Rất khả chuyển, chạy được trên nhiều nền tảng mà không phải biên dịch lại (tất nhiên rồi) hay sửa code 

II. Khác nhau
=============

1. PHP hơn
----------

* PHP kế thừa các curly braces để đánh dấu code block của C và dấu ``$`` của Perl 
* Các câu lệnh ``switch`` và vòng lặp ``do/while``
* Toán tử gán, increment và decrement (``++`` và ``--``) 
* Toán tử ternary operator/statement (``… ? … : …``) 
  * Trong Python 2.5 có câu lệnh ``if/else`` có tác dụng tương tự (``X if C else Y``)
* Array rất mạnh, dùng cho cả list và dict 
* Tham chiếu (``$a =& $b`` tức là khi biến ``$b`` thay đổi, thì biến ``$a`` cũng thay đổi) 
* Có cả private, protected và public access modifiers cho cả phương thức và thuộc tính 
* Dùng cả abstract và final modifiers cho cả classes và methods 
* Có interfaces 
* ``$this`` là đối tượng mặc định, và không cần truyền ngược ``self`` để định nghĩa method như Python (với Python, để định nghĩa ``test`` là method của một lớp cần định nghĩa: ``def test(self)``)

2. Python hơn
-------------

* Ngôn ngữ sử dụng với các mục đích chung (Python có thể dùng để lập trình hầu hết mọi thứ, trong khi PHP chỉ dùng để lập trình web hay console. Tất nhiên có thể làm được việc khác nhưng không đáng kể) 
* Sử dụng indentation (thụt vào đầu dòng, và dòng mới) thay thế cho các dấu curly braces để đánh dấu các code block. Tất nhiên là trông đẹp hơn, nhưng không thân thiện với HTML template cho Web) 
* Có namespaces và modules (PHP 5.3 mới có namespaces) 
* Truyền biến cho hàm hiệu quả hơn PHP nhờ cặp ``key=value`` pair, hỗ trợ tốt hơn cho giá trị mặc định của biến truyền vào hàm
* Cho phép đa thừa kế (không rõ là hay hay là dở nữa) 
* Introspection tốt hơn PHP Reflection 
* Mọi thứ từ biến tới đối tượng đều có kiểu tham chiếu 
* Có Threading 
* Document tốt hơn PHP (nhưng chắc chắn là ít tài liệu trên mạng hơn PHP) 
* Có thể lập trình GUI (Tất nhiên là PHP cũng có thể nhưng không thể bằng) 
* Có thể chạy trên .Net với IronPython hay nền Java với Jython (PHP cũng có thể chạy trên Java thậm chí trong cả các Application Server) 
* Có web server, application server riêng, 100% viết bằng Python.
* Có lambda và các hàm hỗ trợ để xây dựng hàm (PHP có create_function và closure) 
* Cached byte-code (PHP cũng có nhưng không tốt bằng) 
* Unicode được hỗ trợ tốt hơn 
* Kiểm soát lỗi ngoại lệ tốt hơn 
* Chạy nhanh hơn PHP (nhanh hơn nhưng tốn CPU và RAM) 

Hãy học Python cho đời còn lại của bạn nhé!

(Bài viết được chuyển từ diễn đàn do bạn kaka đóng góp)
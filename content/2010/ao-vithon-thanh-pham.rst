********************
Áo Vithon thành phẩm
********************


:author: Nhóm PCNV
:category: Tin tức
:date: 2010-11-24 05:26:00
:tags: t-shirt, vithon


Sau một số trục trặc không lường trước, nhóm PCNV cũng đã nhận được áo Vithon thành phẩm từ nhà in. Nhóm xin được hân hạnh giới thiệu với các bạn một số ảnh chụp áo thật sau:

.. figure:: http://va8.upanh.com/thumbpic/16.734.21073133.eds0/img20101124110821.700x0.jpg

  `Mặt trước <http://ca8.upanh.com/16.734.21073133.eds0/img20101124110821.jpg>`_

.. figure:: http://va9.upanh.com/thumbpic/16.734.21073134.MGr0/img20101124110836.700x0.jpg

  `Mặt sau <http://ca9.upanh.com/16.734.21073134.MGr0/img20101124110836.jpg>`_

Nhóm PCNV xin cảm ơn nhóm mẫu Mập Ốm đã mua ủng hộ hai chiếc áo cỡ L và S để tạo ra được hai bức hình đẹp "rạng ngời" này. Một số người đã mặc áo đa số đều có chung nhận định áo mặc **thoải mái lắm**.

Áo sẽ được bán với giá **185.000 đồng** cho những người đang đi làm, và **150.000 đồng** cho các bạn học sinh, sinh viên đại học trở xuống. Tại TPHCM, mong các bạn ghé địa chỉ 224 Nguyễn Thái Bình, phường 12, quận Tân Bình, trong giờ làm việc từ thứ hai đến thứ sáu mỗi tuần. Các bạn ở tỉnh khác xin vui lòng liên lạc với nhóm PCNV.

Áo Vithon cũng sẽ được sử dụng làm **giải thưởng** cho những kỳ thi sắp tới của nhóm PCNV. Mong các bạn hăng hái tham gia giựt giải.
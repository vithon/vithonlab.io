*****************************
Giải phụ bách phát bách trúng
*****************************


:author: Nguyễn Thành Nam
:category: Tin tức
:date: 2010-12-13 08:08:00
:tags: 


Cuộc tranh tài `Bắn Suồng <http://www.vithon.org/2010/12/13/kết-quả-bắn-suồng>`_ tuy đã kết thúc nhưng dư âm (và cả bao nỗi niềm ấm ức, tức tưởi) của nó vẫn còn vấn vương trong tâm trí các bạn tham gia.

Vì vậy, mình xin đề nghị một giải phụ của Bắn Suồng với thể lệ như sau:

#. Nếu bạn tạo được một suồng "bách phát bách trúng", mình sẽ tặng áo với chữ ký của người sáng lập nhóm PCNV. Điều này cũng có nghĩa là có thể có nhiều hơn một bài thắng giải.
#. "Bách phát bách trúng" được hiểu theo nghĩa một phát bắn phải trúng ít nhất một tàu.
#. Bạn có thể sử dụng mọi kỹ thuật (mũ đen hay trắng) để làm việc này.
#. Thời hạn nhận bài là hết ngày 19 tháng 12 năm 2010.
#. Bài tham gia phải sử dụng ngôn ngữ Python làm ngôn ngữ chính, và nộp mã nguồn.
#. Bài tham gia gửi về cho admin+thư+rác@vithon.org (nhớ bỏ phần chống spam phía sau nhé).
#. Mình xin giữ toàn quyền quyết định kết quả.

Chúc may mắn.
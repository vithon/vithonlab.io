**************************************
Hình ảnh tại Software Freedom Day 2010
**************************************


:author: Nguyễn Thành Nam
:category: Tin tức
:date: 2011-10-24 00:50:00
:tags: member, forum


Hôm nay tự nhiên thấy trên máy có mấy tấm hình cũ ở Ngày Phần Mềm Tự Do 2010 vẫn chưa được đăng.

Năm 2010, thành viên Phan Đắc Anh Huy đã trình bày về phần mềm Vithon Forum (chính là phần mềm được sử dụng để làm Diễn đàn của nhóm PCNV). Sau đây là các tấm hình chụp hôm đó.

.. figure:: http://img36.imageshack.us/img36/1311/img20100918104740.th.jpg

  http://img36.imageshack.us/i/img20100918104740.jpg/

.. figure:: http://img37.imageshack.us/img37/9138/img20100918104748.th.jpg

  http://img37.imageshack.us/i/img20100918104748.jpg/

.. figure:: http://img97.imageshack.us/img97/2699/img20100918104833.th.jpg

  http://img97.imageshack.us/i/img20100918104833.jpg/

Điều kiện ánh sáng tuy không được tốt nhưng cũng phần nào thể hiện được cái đầu du côn và thân hình phì lũ của Huy.

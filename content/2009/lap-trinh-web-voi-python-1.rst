****************************
Lập trình web với Python (1)
****************************


:author: Nguyễn Thành Nam
:category: Bài viết
:date: 2009-12-17 09:07:00
:tags: apache, install


Trong loạt bài lập trình web với Python, tôi sẽ hướng dẫn bạn đọc những thao tác cơ bản từ việc cài đặt, cấu hình, cho đến lập trình, hoàn thành một ứng dụng web đơn giản. Qua quá trình đó, bạn đọc sẽ được giới thiệu về kỹ thuật CGI, đến FastCGI và cuối cùng là WSGI.

Cài đặt máy chủ web Apache
--------------------------

Để lập trình web, chúng ta cần một máy chủ phục vụ web. Máy chủ `Apache HTTP <http://httpd.apache.org/>`_ là một trong những phần mềm máy chủ web phổ dụng trên Internet. Chúng ta sẽ cần tải bản cài đặt phần mềm này về. Tôi sẽ sử dụng phiên bản 2.2.14 (``apache_2.2.14-win32-x86-openssl-0.9.8k.msi``) cho các bài viết trong loạt bài này. Hệ điều hành tôi đang sử dụng là Windows 7.

Khi chạy tập tin cài đặt, chúng ta sẽ gặp giao diện như sau:

.. image:: /static/web-programming/install-apache/apache-install-1.png

Nhấn vào nút **Next** để qua màn hình kế:

.. image:: /static/web-programming/install-apache/apache-install-2.png

Chọn ô đầu tiên để đồng ý với cam kết bản quyền, và nhấn nút **Next** để qua trang kế:

.. image:: /static/web-programming/install-apache/apache-install-3.png

Tiếp tục nhấn nút **Next**:

.. image:: /static/web-programming/install-apache/apache-install-4.png

Điền thông tin vào các ô trống như trong hình ví dụ, và nhấn **Next**:

.. image:: /static/web-programming/install-apache/apache-install-5.png

Chọn kiểu cài đặt **Typical** và nhấn nút **Next**:

.. image:: /static/web-programming/install-apache/apache-install-6.png

Nếu bạn muốn thay đổi đường dẫn cài đặt thì bạn sẽ chỉnh trong trang này, còn không thì nhấn nút **Next**:

.. image:: /static/web-programming/install-apache/apache-install-7.png

Nhấn nút **Install** để bắt đầu quá trình cài đặt:

.. image:: /static/web-programming/install-apache/apache-install-8.png

Chúng ta sẽ được thông báo về quá trình cài đặt.

.. image:: /static/web-programming/install-apache/apache-install-9.png

Sau khi cài đặt hoàn thành, nhấn **Finish** để kết thúc:

.. image:: /static/web-programming/install-apache/apache-install-10.png

Nhấn vào nút **Start** và nhập vào **services.msc** như hình trên để kiểm tra sự tồn tại của máy chủ web Apache2:

.. image:: /static/web-programming/install-apache/apache-install-11.png

Như hình trên, dịch vụ Apache2 đã được chạy (started). Để kiểm tra thì chúng ta sẽ mở Internet Explorer (IE) lên và truy cập vào địa chỉ **http://localhost**. Nếu bạn nhận được kết quả như hình bên dưới đây thì bạn đã hoàn thành việc cài đặt máy chủ web Apache rồi đó.

.. image:: /static/web-programming/install-apache/apache-install-12.png

Trong bài tới chúng ta sẽ tiếp tục thực hiện việc cài đặt Python.
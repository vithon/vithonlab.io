******************************
Làm việc với String (Python 3)
******************************


:author: Nhóm PCNV
:category: Bài viết
:date: 2011-02-09 04:46:00
:tags: string


So sánh cơ bản:

.. code-block:: pycon

  >>> A="caulacbopython"
  >>> B="pythonviet"
  >>> print(A==B)
  False
  >>> print(A>B)
  False
  >>> print(A<B)
  True

Ghép chuỗi: 

.. code-block:: pycon

  >>> A="Yeu"
  >>> B="Python"
  >>> A+B
  'YeuPython'

Split: Hiểu nôm na, ta có một cái chuỗi dài. Giờ ta sẽ cắt nó thành nhiều khúc dài dài và sử dụng khúc nào đó thì tùy:

.. code-block:: pycon

  >>> A="Cau-Lac-Bo-Python-Viet"
  >>> B=A.split('-')
  >>> B
  ['Cau', 'Lac', 'Bo', 'Python', 'Viet']
  >>> B[0]
  'Cau'
  >>> B[1]
  'Lac'
  >>> B[2]
  'Bo'

Lấy 1 ký tự bất kì:

.. code-block:: pycon

  >>> chuoi= "NguyenMinhThe"
  >>> print(chuoi[2])
  u
  >>> print(chuoi[1])
  g

Thay thế (Replace): 

.. code-block:: pycon

  >>> chuoi="MinhThe"
  >>> print(chuoi.replace('h','H'))
  >>> 'MinHTHe'

Đếm số lần xuất hiện ký tự trong 1 chuỗi: 

.. code-block:: pycon

  >>> ch="VithonViet"
  >>> print(ch.count('V'))
  2

Trích từ bài gửi diễn đàn của **Nguyễn Minh Thế**.

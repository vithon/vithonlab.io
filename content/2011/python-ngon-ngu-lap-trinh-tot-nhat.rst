************************************
Python - Ngôn Ngữ Lập Trình Tốt Nhất
************************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2011-12-09 19:13:00
:tags: python


Tạp chí Linux Journal đã công bố kết quả khảo sát người đọc hàng năm của mình vào ngày 01 tháng 12 vừa qua. Theo đánh giá này, Python lại một lần nữa trong suốt ba năm liên tục được người đọc đánh giá là **ngôn ngữ lập trình tốt nhất**.

Thông tin chi tiết có thể được xem thêm từ nguồn `Linux Journal <http://www.linuxjournal.com/slideshow/readers-choice-2011?page=27>`_.
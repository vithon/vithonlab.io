***************************
Giới thiệu áo của nhóm PCNV
***************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2010-09-10 04:17:00
:tags: t-shirt


Trong mấy tuần vừa qua, nhóm PCNV đã huy động toàn bộ sức lực để hoàn thành mẫu thiết kế áo thun chính thức của nhóm. Xin hân hạnh giới thiệu cùng các bạn thiết kế đầy ấn tượng này.

.. figure:: /static/ao_vithon_truoc_thumb.png

  `Mặt trước </static/ao_vithon_truoc.png>`_

.. figure:: /static/ao_vithon_sau_thumb.png

  `Mặt sau </static/ao_vithon_sau.png>`_

Hiện áo đang được đưa đi in với số lượng **50** chiếc. Nếu bạn có nhu cầu muốn đặt mua áo thì vui lòng liên lạc với nhóm PCNV trong `diễn đàn <forum.vithon.org>`_, hoặc qua kênh IRC **#vithon** ở máy chủ **irc.freenode.net** (http://webchat.freenode.net). Giá dự tính của mỗi chiếc áo sẽ ít hơn **một trăm năm mươi nghìn đồng**. Do số lượng có hạn nên áo sẽ được phân phát theo phương thức *nhanh tay thì còn*.

**Nhóm PCNV không chịu trách nhiệm nếu người mặc áo bị các lập trình viên khác gây chuyện!**

Nhóm PCNV trân trọng cảm ơn công sức thâu đêm suốt sáng nhiều tuần liền của các thành viên năng nổ sau:

Kịch bản
  Hoàng Quốc Thịnh

Đạo diễn
  Nguyễn Thành Nam

Hiệu ứng
  Lê Ngọc Hiếu

Sản xuất
  Phan Đắc Anh Huy

Chỉ đạo nghệ thuật
  Tập thể diễn viên hài PCNV

Chúc các bạn vui vẻ!

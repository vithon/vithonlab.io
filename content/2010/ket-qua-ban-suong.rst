*****************
Kết quả Bắn Suồng
*****************


:author: Nguyễn Thành Nam
:category: Tin tức
:date: 2010-12-13 07:54:00
:tags: 


Ngày hôm qua tại Đại học RMIT, nhóm PCNV đã tổ chức thi Bắn Suồng trực tiếp (cùng với việc truyền hình trực tiếp) cuộc tranh tài Bắn Suồng.

Kỳ thi này nhận được bốn bài tham dự: **fire_sheep**, **checker**, **randome**, và **bibo**. Tuy nhiên, bài **bibo** vì gửi vào 0152 sáng 12/12 nên đã bị loại.

Kết quả trận tranh tài căng thẳng, và đầy hồi hộp (lẫn may mắn) là:

#. **fire_sheep** thắng **checker**
#. **fire_sheep** thua **randome**
#. **checker** thua **randome**

Như vậy, "suồng" randome đã đăng quang vô địch kỳ thi lần này. Tác giả của **randome**, không ai khác, chính là tác giả của tin này :-D.

Hẹn gặp các bạn vào những kỳ thi thú vị tới.

Tái bút: Mã nguồn của Bắn Suồng và các bài tranh tài có thể được tải về tại http://www.vithon.org/static/ban-suong.zip.
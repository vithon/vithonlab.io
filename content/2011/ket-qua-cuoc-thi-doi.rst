********************
Kết quả cuộc thi Đời
********************


:author: Nhóm PCNV
:category: Tin tức
:date: 2011-10-21 10:51:00
:tags: python, contest


Cuộc thi Đời kết thúc vào tuần trước nhưng vì không có điều kiện truy cập Internet nên đến bây giờ ban tổ chức mới có thể công bố kết quả.

Cuộc thi thu hút **bốn** bài tham dự từ các bạn sau:

=================  ===============
**Tác giả**        **Bài tham dự**
=================  ===============
Nhâm Xuân Nam      life.py
Huỳnh Hải Âu       bilife.py
Phan Phụng Tiến    treeoflife.py
Vũ Khuê            cuocthidoi.py
=================  ===============

Nhóm PCNV đã sử dụng một chương trình chấm giải đơn giản khác để đánh giá các bài này. Chương trình này chỉ đơn giản là tạo một ma trận MxN phần tử và yêu cầu các bài tham gia tính giá trị của các ô ngẫu nhiên tại một thế hệ trong tương lai.

Ở vòng đầu tiên, ma trận **1x1** được sử dụng::

  G:\vithon\life>judge.py -c 1 -r 1 -n 10000 bilife.Board life.MainBoard treeoflife.Board cuocthidoi.Board
  Generating random test case
  Width = 1
  Height = 1

  0

  Executing bilife.Board
  Executing life.MainBoard
  Executing treeoflife.Board
  Executing cuocthidoi.Board

  Checking for cell (0, 0)
  Traceback (most recent call last):
    File "G:\vithon\life\judge.py", line 120, in <module>
      main()
    File "G:\vithon\life\judge.py", line 96, in main
      if results[module_name]['object'].get_cell(row, col):
    File "G:\vithon\life\treeoflife.py", line 160, in get_cell
      raise ValueError()
  ValueError

Vì sinh ra lỗi nên mô-đun **treeoflife** đã bị loại.

Ở vòng hai, ma trận **2x2** được sử dụng và chỉ tính một thế hệ tương lai::

  G:\vithon\life>judge.py -c 2 -r 2 -n 1 bilife.Board life.MainBoard cuocthidoi.Board
  Generating random test case
  Width = 2
  Height = 2

  1 1
  0 1

  Executing bilife.Board
  Executing life.MainBoard
  Executing cuocthidoi.Board

  Checking for cell (0, 1)
  *** alive count = 2, dead_count = 1
  bilife is eliminated

  Checking for cell (1, 0)
  *** alive count = 3, dead_count = 0

  Checking for cell (0, 0)
  *** alive count = 2, dead_count = 1
  bilife is eliminated

  Checking for cell (1, 1)
  *** alive count = 2, dead_count = 1
  bilife is eliminated

  RESULT:
  [+] cuocthidoi : 0.000000 seconds
  [+] life : 0.000000 seconds
  [-] bilife : 0.000000 seconds

Với lần chạy này, chỉ có mô-đun **bilife** đưa ra kết quả sai cho trường hợp ô **(0, 1)**. Chương trình chấm so sánh kết quả của mô-đun với kết quả của **số đông**. Trong trường hợp này, số đông đã đúng.

Sau hai lần chạy, chỉ còn lại hai mô-đun **life** và **cuocthidoi**.

Lần chạy thứ ba sử dụng ma trận **2x2** với một thế hệ::

  G:\vithon\life>judge.py -c 2 -r 2 -n 1 life.MainBoard cuocthidoi.Board
  Generating random test case
  Width = 2
  Height = 2

  1 0
  0 0

  Executing life.MainBoard
  Executing cuocthidoi.Board

  Checking for cell (0, 0)
  *** alive count = 1, dead_count = 1
  life is eliminated

  Checking for cell (1, 1)
  *** alive count = 0, dead_count = 2

  Checking for cell (1, 0)
  *** alive count = 0, dead_count = 2

  Checking for cell (1, 0)
  *** alive count = 0, dead_count = 2

  RESULT:
  [-] life : 0.000000 seconds
  [+] cuocthidoi : 0.000000 seconds

Với lần chạy này, **life** đã bị loại, và chỉ còn **cuocthidoi**.

Tóm lại, sau ba vòng chạy thì chỉ có mô-đun **cuocthidoi** đưa ra đáp án đúng. Tuy nhiên, giải này không có người chiến thắng vì mục tiêu của giải là thi về tốc độ thực thi. Khi chỉ có một đối thủ trong cuộc thi thì giải trở nên vô nghĩa. Hơn nữa, bài tham dự **cuocthidoi.py** đã không thực hiện đúng quy chế thứ 3 trong thể lệ tham gia nên cũng bị loại. Mặc dù không có người chiến thắng nhưng nhóm PCNV xin chúc mừng tác giả **Vũ Khuê** đã vượt qua các đối thủ khác trong cuộc thi.

Nói tóm lại, giải đã không đạt được mục tiêu đề ra ban đầu. Xin hẹn gặp các bạn ở các giải sau!

*******************
Thomas Heller ra đi
*******************


:author: Nhóm PCNV
:category: Tin tức
:date: 2011-03-21 03:28:00
:tags: quit, ctypes, thomas heller


Ngày 19 tháng 03, Thomas Heller đã tự đề nghị nhóm phát triển Python tước quyền cập nhật của ông.

Thomas Heller là tác giả gói **ctypes**, được sử dụng rất nhiều trong việc kết nối Python với các thư viện ngoài.

Thomas ra đi vì không còn nhiều thời gian để cập nhật **ctypes** nữa. Và điều đó để lại một lỗ hổng to trong việc duy trì, sửa lỗi mô-đun quan trọng này.

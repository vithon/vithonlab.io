*************************
Python 2.7 beta 1 ra mắt 
*************************


:author: Võ Đức Phương
:category: Tin tức
:date: 2010-04-18 11:00:00
:tags: 


Python 2.7 là phiên bản cuối cùng của Python 2 trước khi chuyển sang chế độ chỉ sửa lỗi, không thêm tính năng vào họ phiên bản này nữa. Tương tự như 2.6, phiên bản này sẽ chú trọng vào việc thêm vào những tính năng của Python 3 (chính xác là Python 3.1). Những cải tiến của phiên bản này bao gồm:

* Thêm loại từ điển có sắp xếp
* Thêm một số tính năng cho unittest như bỏ qua kiểm tra hay các phương pháp kiểm định mới.
* Mô đun ``io`` mới cho tốc độ nhanh hơn
* Tự đánh số các trường với phương pháp ``str.format()``
* Mô đun ``sysconfig`` mới.

... cùng nhiều thay đổi khác nữa. Thông tin chi tiết về phiên bản này có thể được xem tại: 

`Có những gì mới trong Python 2.7 <http://docs.python.org/dev/whatsnew/2.7.html>`_

`Danh sách các thay đổi của Python 2.7 <http://svn.python.org/projects/python/tags/r27b1/Misc/NEWS>`_

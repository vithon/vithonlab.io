***************************
Lập lịch gọi hàm với Gevent
***************************


:author: Nguyễn Thành Nam
:category: Bài viết
:date: 2011-08-22 07:29:00
:tags: cron, gevent, schedule


Đôi khi chúng ta cần một cách gì đó để lập lịch gọi hàm tương tự như công cụ ``cron`` trong các hệ Unix. Nếu chúng ta may mắn đang làm việc với Gevent sẵn rồi thì hai dòng sau là một cách đơn giản để lập lịch.

.. code-block:: python

  def schedule(delay, func, *args, **kw_args):
    gevent.spawn_later(0, func, *args, **kw_args)
    gevent.spawn_later(delay, schedule, delay, func, *args, **kw_args)

Ý tưởng đằng sau hai dòng lệnh này là chúng ta tận dụng sẵn vòng lặp sự kiện (event loop) của Gevent để cài hàm cần được thực thi vào. Do đó, chúng ta sẽ đỡ được việc sử dụng vòng lặp với ``sleep``. Dòng thứ hai có tác dụng như một lệnh gọi đệ quy đuôi (tail recursion), nhưng không tốn bộ nhớ ngăn xếp (stack memory).

Trong trường hợp chúng ta không sử dụng Gevent thì gói `apscheduler <http://packages.python.org/APScheduler/>`_ là một lựa chọn đáng tham khảo khác. Gói này cung cấp dịch vụ lập lịch tương tự như Quartz trong thế giới Java.

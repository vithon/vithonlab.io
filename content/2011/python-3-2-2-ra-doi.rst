*******************
Python 3.2.2 ra đời
*******************


:author: Nhóm PCNV
:category: Tin tức
:date: 2011-09-05 07:03:00
:tags: python, release


Khoảng 10 tiếng trước, Georg Brandl đã gửi một bức thư điện tử lên hộp thư chung python-dev để công bố sự ra đời của phiên bản Python 3.2.2.

Python 3.2.2 về cơ bản là phiên bản sửa lỗi, đặc biệt là lỗi `trong mô-đun urllib.request <http://bugs.python.org/12576>`_ xảy ra trong 3.2.1.

Phiên bản 3.2.2 có thể được tải về từ địa chỉ http://www.python.org/download/releases/3.2.2/.

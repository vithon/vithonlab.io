**************************************
Kết quả cuộc thi Giải Toán Bằng Python
**************************************


:author: Phan Đắc Anh Huy
:category: Tin tức
:date: 2010-06-03 04:04:00
:tags: 


Sau một tuần tranh tài sôi nổi, cuộc thi Giải Toán Bằng Python mừng ngày Quốc Tế Thiếu Nhi đã kết thúc tốt đẹp.

Ngày 1/6, với sự tham dự của hầu hết các thí sinh và cổ động viên, anh Nguyễn Thành Nam - trưởng nhóm Python cho người Việt - đã tiến hành chấm bài. Đúng như dự đoán trong thời gian diễn ra kỳ thi của anh, mặc cho sự cạnh tranh rất quyết liệt giữa các thí sinh, vẫn có một bài thi tỏ ra vượt trội và giành chiến thắng một cách thuyết phục. Đó là bài dự thi của bạn Nhâm Xuân Nam, với nickname namnx.

Với nội dung thi gần gũi, dễ tiếp cận nhưng cũng không kém phần hấp dẫn, cuộc thi đã thu hút sự tham gia đông đảo hơn rất nhiều so với hai cuộc thi trước. Chất lượng cuộc thi rất cao khi hầu hết các thí sinh đều tìm ra được công thức cho phép tính nhanh các cặp số, một số kỹ thuật về multi-processing cũng được các thí sinh tìm hiểu và tận dụng rất hiệu quả. Bài của bạn Nhâm Xuân Nam đã đạt được kết quả cách biệt với số ``n`` lớn nhất dài hơn 2100 chữ số!

Nói về bài thi đoạt giải nhất, bạn Hoàng Quốc Thịnh - người đạt giải nhất cuộc thi đầu tiên - và anh Nguyễn Thành Nam đều chung một nhận định:

  Một bài thi chuẩn mực, không có gì phải phàn nàn cả!

Mã nguồn của các bài dự thi cùng với chương trình chấm có thể được tải về tại `đây <http://www.vithon.org/static/pell.zip>`_. Hình sau là ảnh của lễ trao giải. Bạn Lê Ngọc Hiếu đại diện cho nhóm PCNV bên phải, trao tặng giải thưởng cho người chiến thắng bên trái.

.. image:: /static/pell.contest.png

Một lần nữa xin chúc mừng bạn Nhâm Xuân Nam!

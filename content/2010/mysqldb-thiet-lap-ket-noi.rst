***************************
MySQLdb - Thiết lập kết nối
***************************


:author: Phạm Thị Minh Hoài
:category: Bài viết
:date: 2010-05-31 16:55:00
:tags: mysql, MySQLdb, CSDL, database connection, database


MySQLdb là một wrapper của thư viện MySQL C API: ``_mysql``. Nó cho phép bạn viết các ứng dụng Python khả chuyển (portable) chạy trên nhiều môi trường khác nhau để truy cập vào máy chủ có hệ quản trị CSDL MySQL.
 
Bài viết sau đây đề cập một số khía cạnh liên quan tới thiết lập kết nối CSDL. Đó là vấn đề quan trọng đầu tiên khi làm việc với bất kỳ hệ quản trị CSDL nào. 

Bản cài đặt MySQLdb có thể download trên trang: http://sourceforge.net/projects/mysql-python/. Download có sẵn cho các nền tảng windows 32 bit và các họ unix. Đáng tiếc là MySQLdb hiện chỉ sử dụng cho các phiên bản python từ 2.3 đến 2.6.  

Hello World
===========

Giả thiết trên máy của bạn đã cài MySQL với cổng mặc định (3306) và account root không cần password. Đoạn chương trình sau minh họa một kết nối đơn giản nhất có thể. Nó in ra các db có trong CSDL này: 

.. code-block:: pycon

  >>> db = MySQLdb.connect(user="root")
  >>> cursor = db.cursor()
  >>> query = "SHOW DATABASES"
  >>> cursor.execute(query)
  6L
  >>> cursor.fetchall()
  (('information_schema',), ('cdcol',), ('mysql',), ('phpmyadmin',), ('test',))

Kết nối theo IP
===============

Truy cập từ xa đến một hệ thống phức tạp hơn, bạn cần biết các tham số:

* Máy chủ chứa MySQL (mặc định: ``localhost`` hay ``127.0.0.1``)
* Cổng truy cập vào MySQL (mặc định: 3306)
* User, Password mà bạn được cấp
* Tên CSDL bạn muốn truy cập

Ví dụ sau in ra các bảng của CSDL ``mydb`` trên máy ``192.168.90.31`` với cổng 3310:

.. code-block:: pycon

  >>> db = MySQLdb.connect(user="you", db="mydb", passwd="mypw", \
        host="192.168.90.31", port=3310)
  >>> cursor = db.cursor()
  >>> query = "SHOW TABLES"
  >>> cursor.execute(query)
  >>> cursor.fetchall()

Kết nối theo UNIX socket
========================

Trên các localhost cho phép dùng UNIX socket bạn có thể viết:

.. code-block:: pycon

  db = MySQLdb.connect(db="test", user="hoaiptm", passwd="hoaiptm", \
                       unix_socket="/opt/hoaiptm/mysql/mysql.sock")

Truy cập MySQL qua các socket trên unix tương tự như named pipe trên windows. Nó cho phép các ứng dụng localhost tạo các kết nối cực kỳ nhanh đến CSDL. Nếu bạn chưa hiểu rõ về socket bạn có thể đọc: http://beej.us/guide/bgipc/output/html/multipage/unixsock.html. 

Giấu password
=============

Các ví dụ trên đây đều yêu cầu phải có password đặt trong mã nguồn Python. MySQLdb cho phép bạn giấu chúng trong một nơi mà chỉ bạn được phép truy cập:

.. code-block:: pycon

  db = MySQLdb.connect(db="yourdb", read_default_file="/home/hoaiptm/mysql.ini")

tập tin options ``/home/hoaiptm/mysql.ini`` có thể có nội dung như sau::

  [client]
  port=3310
  socket=/opt/hoaiptm/mysql/mysql.sock
  user=hoaiptm
  password=hoaiptm

Hướng dẫn về các option này có thể đọc trong tài liệu: http://www.yellis.net/docs/mysql/manuel_Option_files.html#Option_files.

Thay đổi charset mặc định
=========================

Các kết nối trong các ví dụ trên sử dụng charset mặt định là **latin1**, do đó bạn không lấy được các dữ liệu tiếng Việt. Chẳng hạn bạn tạo một bảng **user** với chartset **utf8** trong CSDL **test** như sau::

  USE `test`;
  CREATE TABLE `user` (
    `Id` int(11) NOT NULL AUTO_INCREMENT,
    `account` varchar(50) DEFAULT NULL,
    `fullname` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`Id`)
  ) ENGINE=MyISAM AUTO_INCREMENT=3 **DEFAULT CHARSET=utf8** ROW_FORMAT=DYNAMIC;
  INSERT INTO `user` VALUES (1,'hoaiptm','Phạm Thị Minh Hoài');
  INSERT INTO `user` VALUES (2,'trangnt','Ngô Thu Trang');

Sau đó cố gắng lấy dữ liệu từ bảng user:

.. code-block:: pycon

  >>> db = MySQLdb.connect(user="hoaiptm", db="test", passwd="hoaiptm", port=3310)
  >>> cursor = db.cursor()
  >>> query = "SELECT fullname FROM user"
  >>> cursor.execute(query)
  2L
  >>> cursor.fetchall()
  (('Ph?m Th? Minh Ho\xe0i',), ('Ng\xf4 Thu Trang',))

Các dấu ``?`` cho thấy các ký tự utf8 đã không được truyền tải đúng. Để giải quyết vấn đề này chúng ta thêm tùy chọn **charset = "utf8"** trong connection:

.. code-block:: pycon

  >>> db = MySQLdb.connect(user="hoaiptm", db="test", passwd="hoaiptm", \
        port=3310, charset="utf8")
  ...
  >>> cursor.fetchall()
  ((u'Ph\u1ea1m Th\u1ecb Minh Ho\xe0i',), (u'Ng\xf4 Thu Trang',))

Lúc này tất cả các dữ liệu trong trường text, char, varchar đều được trả về dạng unicode.

Yêu cầu trả về từ điển
======================

MySQLdb cho phép trả về các kết quả dạng từ điển, để làm được điều đó bạn thêm tùy chọn **cursorclass=MySQLdb.cursors.DictCursor**. Ví dụ:

.. code-block:: pycon

  >>> db = MySQLdb.connect(user="hoaiptm", db="test", passwd="hoaiptm", port=3310, \
        charset="utf8", cursorclass=MySQLdb.cursors.DictCursor)
  >>> cursor = db.cursor()
  >>> query = "select * from user"
  >>> cursor.execute(query)
  2L
  >>> cursor.fetchall()
  ({'account': u'hoaiptm', 'fullname': u'Ph\u1ea1m Th\u1ecb Minh Ho\xe0i', 'Id': 1L}, \
   {'account': u'trangnt', 'fullname': u'Ng\xf4 Thu Trang', 'Id': 2L})

Chương trình sẽ chạy chậm đi chút xíu, nhưng bù lại kết quả trả về rất dễ sử dụng.
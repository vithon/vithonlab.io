beautifulsoup4==4.9.3
docutils==0.16
pelican==4.5.3
typogrify>=2.0.7
MarkupSafe==1.1.1

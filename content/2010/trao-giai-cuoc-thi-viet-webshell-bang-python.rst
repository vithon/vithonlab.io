********************************************
Trao giải cuộc thi viết webshell bằng Python
********************************************


:author: Lê Ngọc Hiếu
:category: Tin tức
:date: 2010-05-17 05:17:00
:tags: 


Ngày 16/05/2010 tại quán cafe K&K đã diễn ra buổi lễ trao giải cho cuộc thi viết webshell bằng python. Tại buổi lễ, anh Nguyễn Thành Nam là trưởng nhóm Python cho người Việt đã trao giải nhất của cuộc thi cho bạn Phan Đắc Anh Huy với phần thưởng là 100.000 đồng.

.. figure:: /static/webshell_programming_contest.jpg

  Hình: anh Nguyễn Thành Nam trao giải cho bạn Phan Đắc Anh Huy

Cuộc thi lần này đã thu hút sự tham gia của nhiều bạn trẻ hơn so với cuộc thi viết game bằng Python lần trước. Theo anh Nam, chất lượng cuộc thi lần này đã khá hơn hẳn lần trước. Điều đó chứng tỏ trình độ lập trình bằng Python và cộng đồng sử dụng Python đã và đang lớn mạnh hơn. Đây cũng chính là mục đích của trang mạng Python cho người Việt được lập ra.

Sau lễ trao giải, nhóm cũng đã có buổi thảo luận sôi nổi về việc sẽ thúc đẩy sinh hoạt gặp mặt trực tiếp nhằm nâng cao hơn nữa tay nghề. Hy vọng đây sẽ là một hoạt động bổ ích và thiết thực cho các bạn trẻ ham học hỏi. Bạn Hoàng Quốc Thịnh - người đã đoạt giải nhất trong cuộc thi lần trước - chia sẻ:

  Mình nghĩ không nên lạm dụng việc sinh hoạt trực tuyến nhiều quá, sinh hoạt trực tiếp gặp mặt có điểm lợi hơn đó là việc cái 'lửa' sẽ được tận mắt nhìn thấy và truyền cho nhau. Điều này, sinh hoạt trực tuyến không có được.

Anh Nam cũng có cùng nhận định như trên và đang chuẩn bị cho những hoạt động đẩy mạnh trong thời gian sắp tới.

K&K, 15:03 16/05/2010

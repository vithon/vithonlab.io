********************************
Phát hành Bài chỉ dẫn Python 2.5
********************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2007-09-25 21:50:00
:tags: tutorial


*Bài chỉ dẫn Python* 2.5 tiếng Việt đã được nhóm Python cho người Việt phát hành vào ngày 26 tháng 09 năm 2007.

Ngày 26 tháng 09 năm 2007, theo đúng như dự định, nhóm Python cho người Việt đã chính thức phát hành bản dịch của tài liệu *Python Tutorial* với tên **Bài chỉ dẫn Python**. Tài liệu này giới thiệu người đọc những khái niệm nhập môn cơ bản của ngôn ngữ Python và các thư viện có sẵn của nó. Người mới học, cũng như các chuyên gia trong ngành Công Nghệ Thông Tin có thể tìm được nhiều thông tin bổ ích từ tài liệu.

Phát biểu về sự kiện này, anh Nguyễn Thành Nam (một trong các dịch giả và là thành viên của nhóm Python cho người Việt) nhận xét:

  Việc có mặt một tài liệu Python tiếng Việt hoàn toàn miễn phí là một dấu mốc quan trọng trong việc đưa ngôn ngữ Python đến gần hơn với người dùng Việt Nam.

Anh cũng cho biết thêm rằng bản dịch này là sự hợp tác của nhiều dịch giả từ khắp nơi trên thế giới có cùng niềm đam mê với ngôn ngữ Python và mong muốn giới thiệu và phát triển nó rộng rãi ở Việt Nam.

Tài liệu có thể được truy cập tại địa chỉ http://www.vithon.org/tutorial/2.5/tut.html.
*********************
Máy học phổ thông (5)
*********************
-----------------------------------------
Hồi quy hậu cần (logistic regression) (3)
-----------------------------------------


:author: Nguyễn Thành Nam
:category: Bài viết
:date: 2018-06-03
:tags: machine learning, logistic regression


Trong hai phần trước, chúng ta giả định rằng giá trị thực :math:`y \in \{ 0, 1 \}`. Bài viết này sẽ tìm hiểu sự thay đổi của hàm mất mát nếu giá trị thực nằm trong tập hợp :math:`\{ -1, 1 \}`.

====  ===========  ===========  =========
Dòng  :math:`x_1`  :math:`x_2`  :math:`y`
====  ===========  ===========  =========
0     0            0            -1
1     0            1            -1
2     1            0            -1
3     1            1            1
====  ===========  ===========  =========


Hàm mất mát với :math:`y \in \{ -1, 1 \}`
=========================================

Các giả định xác suất
---------------------

Tương tự như trong `bài trước <{filename}/2018/may-hoc-pho-thong-4.rst>`_, chúng ta vẫn giả sử với cùng ý nghĩa xác suất như sau:

.. math::

  \hat{y}_\vec{\theta}(\vec{x}) &= S(\vec{\theta} \cdot \vec{x}) \\
  P_\vec{\theta}(y=1 \mid \vec{x}) &= \hat{y}_\vec{\theta}(\vec{x})

Tuy nhiên, có một thay đổi nhỏ ở giả định thứ ba:

.. math::

  P_\vec{\theta}(y=-1 \mid \vec{x}) &= 1 - P_\vec{\theta}(y=1 \mid \vec{x}) \\
      &= 1 - \hat{y}_\vec{\theta}(\vec{x}) \\
      &= \hat{y}_\vec{\theta}(-\vec{x})

Lưu ý rằng :math:`\hat{y}` không còn mang ý nghĩa là giá trị phỏng đoán nữa! Trong trường hợp này, :math:`\hat{y}` là xác suất của giá trị thực có giá trị là :math:`1`. Nói cách khác, nếu :math:`\hat{y} > 0.5`, giá trị phỏng đoán sẽ là :math:`1`, còn nếu không thì giá trị phỏng đoán sẽ là :math:`-1`.

Thiết lập hàm mất mát
---------------------

Từ các giả sử trên, chúng ta có xác suất của biến phụ thuộc trùng với giá trị thực là:

.. math::

  P_\vec{\theta} \left( y = y^{(i)} \mid \vec{x^{(i)}} \right) = \begin{cases}
    \hat{y}_\vec{\theta}\left( \vec{x^{(i)}} \right) & \mbox{khi } y^{(i)} = 1 \\
    \hat{y}_\vec{\theta}\left( \vec{-x^{(i)}} \right) & \mbox{khi } y^{(i)} = -1 \\
  \end{cases}

Hai trường hợp này có thể được gộp lại làm một như sau:

.. math::

  P_\vec{\theta} \left( y = y^{(i)} \mid \vec{x^{(i)}} \right)
      &= \hat{y}_\vec{\theta}\left( y^{(i)} \times \vec{x^{(i)}} \right) \\
      &= S \left( \vec{\theta} \cdot y^{(i)} \times \vec{x^{(i)}} \right)

Như vậy, hàm hợp lý của mô hình sẽ là:

.. math::

  \mathcal{P}_\vec{\theta} &= \prod_{i=0}^{3} P_\vec{\theta} \left( y = y^{(i)} \mid \vec{x^{(i)}} \right) \\
      &= \prod_{i=0}^{3} S \left( y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}} \right)

Nếu lấy :math:`\log` của hai vế, ta có:

.. math::

  \log\mathcal{P}_\vec{\theta} &= \sum_{i=0}^{3}\log S \left( y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}} \right) \\
      &= \sum_{i=0}^{3} \left( \cfrac{1}{1 + e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}}} \right) \\
      &= \sum_{i=0}^{3}\log \left[ \left( 1 + e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}} \right)^{-1} \right] \\
      &= \sum_{i=0}^{3}-\log \left( 1 + e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}} \right) \\
      &= - \sum_{i=0}^{3}\log \left( 1 + e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}} \right)

Lập luận tương tự như trong `phần trước <{filename}/2018/may-hoc-pho-thong-4.rst>`_, và nhớ rằng giá trị của hàm xích-ma chuẩn bị chặn trong khoảng :math:`(0, 1)`, ta có :math:`\log\mathcal{P}_\vec{\theta}` là một hàm lõm, và :math:`-\log\mathcal{P}_\vec{\theta}` là một hàm lồi. Hàm lồi này chính là hàm mất mát chúng ta cần tìm giá trị cực tiểu.

.. math::

  \mathcal{L}_\vec{\theta} = -\log \mathcal{P}_\vec{\theta} = \sum_{i=0}^{3}\log \left( 1 + e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}} \right)

Dốc của hàm mất mát:

.. math::

  \nabla \mathcal{L}_\vec{\theta} &= \cfrac{\partial \mathcal{L}_\vec{\theta}}{\partial \vec{\theta}} \\
    &= \sum_{i=0}^3\cfrac{1}{1 + e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}}}\cfrac{\partial}{\partial \vec{\theta}} \left( 1+ e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}}\right) \\
    &= \sum_{i=0}^3\cfrac{e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}}}{1 + e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}}}\cfrac{\partial}{\partial \vec{\theta}}\left( -y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}} \right) \\
    &= -\sum_{i=0}^3\cfrac{e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}}}{1 + e^{-y^{(i)}\vec{\theta}\cdot\vec{x^{(i)}}}}y^{(i)}\vec{x^{(i)}} \\
    &= -\sum_{i=0}^3 S \left( -y^{(i)}\vec{x^{(i)}}\cdot\vec{\theta} \right) y^{(i)}\vec{x^{(i)}}


Cài đặt
=======

.. code-block:: python

  # encoding: utf-8
  import math
  import random
  
  def sigmoid(x):
      return 1.0 / (1.0 + math.exp(-x))
  
  def dot(x, theta):
      return sum(x_i * theta_i for x_i, theta_i in zip(x, theta))
  
  def f_prime(theta, x, y):
      return [-(sigmoid(-y * dot(x, theta))) * x_i * y for x_i in x]
  
  def loss(theta, xs, ys):
      s = 0.0
      for i in range(len(ys)):
          x = xs[i]
          y = ys[i]
          s += math.log(1 + math.exp(-y * dot(theta, x)))
      return s
  
  # Bảng đầu vào.
  xs = (
      (1, 0, 0),
      (1, 0, 1),
      (1, 1, 0),
      (1, 1, 1),
  )
  ys = (-1, -1, -1, 1)
  # Khởi tạo theta ngẫu nhiên.
  theta = [random.random() * 2 - 1 for _ in range(len(xs[0]))]
  # Định tốc độ học.
  alpha = 0.5
  
  # Lặp xuống dốc.
  for _ in range(10000):
      # Đạo hàm riêng đối với mỗi dòng đầu vào.
      grad = [f_prime(theta, x, y) for x, y in zip(xs, ys)]
      # Lấy tổng các đạo hàm riêng lại với nhau.
      grad = [sum(g[c] for g in grad) for c in range(len(xs[0]))]
      # Cập nhật theta.
      theta = [theta_i - alpha * grad_i for theta_i, grad_i in zip(theta, grad)]
  
  # In kết quả.
  print('Theta', theta)
  print('Loss', loss(theta, xs, ys))
  for x in xs:
      print(x[1:], 1 if sigmoid(dot(theta, x)) > 0.5 else -1)

Thực thi đoạn mã trên sẽ đem lại kết quả tương tự như trong bài viết trước::

  ('Theta', [-20.371450940891446, 13.468671765331806, 13.468671765331887])
  ('Loss', 0.0034155464217680244)
  ((0, 0), -1)
  ((0, 1), -1)
  ((1, 0), -1)
  ((1, 1), 1)


Một số nhận xét mở rộng
=======================

Hàm mất mát
-----------


Qua ba bài viết về hồi quy hậu cần, chúng ta đã tham khảo ba hàm mất mát khác nhau. Phần đầu tiên sử dụng sai số giữa giá trị thực và giá trị phỏng đoán. Phần thứ hai sử dụng **sai số en-trô-pi chéo** (cross entropy error). Và bài viết kỳ này sử dụng một dạng tương thích khác. Điều quan trọng nhất là chúng ta cần chọn (hoặc tìm) hàm mất mát *phù hợp với hoàn cảnh sử dụng*.

Tìm cực đại của hàm hợp lý
--------------------------

Trong bài trước và bài này chúng ta đều đi đến mục tiêu cuối cùng là tìm giá trị cực đại của hàm hợp lý. Kỹ thuật này được biết đến với tên gọi **ước lượng hợp lý cực đại** (maximum likelihood estimator). Để tìm điểm cực đại của hàm hợp lý, ta đi tìm điểm cực tiểu của số đối của hàm hợp lý. Hơn thế nữa, vì hàm hợp lý là một tích của rất nhiều số nhỏ hơn :math:`1`, chúng ta đã thay thế hàm hợp lý bằng :math:`\log` của hàm hợp lý, và đi tìm giá trị cực tiểu của **số đối của log của hàm hợp lý** (negative log likelihood).


Tóm tắt
=======

Bài viết kỳ này thiết lập hàm mất mát trong trường hợp giá trị thực :math:`y \in \{ -1, 1 \}`. Đây là một dạng phổ biến khác trong hồi quy hậu cần. Tương tự như bài viết kỳ trước, chúng ta đã áp dụng ý nghĩa xác suất vào hàm xích-ma chuẩn và kỹ thuật ước lượng hợp lý cực đại để thiết lập nên hàm mất mát. Sau khi cài đặt, chúng ta thấy rằng kết quả của hồi quy hậu cần trong bài viết này tương tự như kết quả chúng ta đã thấy trong bài viết kỳ trước.

Từ ba bài viết về hồi quy hậu cần, chúng ta cũng rút ra một số nhận xét tổng quát hóa vai trò của hàm mất mát, và kỹ thuật tìm giá trị cực tiểu của số đối của :math:`\log` của hàm hợp lý.

Trong bài viết sau, chúng ta sẽ xem xét một số áp dụng của hai phương pháp học có giám sát đã trình bày trong các bài viết trước. Sau đó chúng ta sẽ quay lại tìm hiểu cách giải quyết vấn đề tham số mô hình bị "phình ra" khi học lâu (qua nhiều bước xuống dốc).


Tài liệu đọc thêm
=================

#. `Bài giảng số 4 <http://www.robots.ox.ac.uk/~az/lectures/ml/2011/lect4.pdf>`_ trong môn `C4B Machine Learning <http://www.robots.ox.ac.uk/~az/lectures/ml/2011/>`_ do Andrew Zisserman dạy ở đại học Oxford.
#. `Tài liệu giảng <https://s3.amazonaws.com/content.udacity-data.com/courses/gt-cse6242/recommended+reading/logreg.pdf>`_ trong môn `CSE6242 Data and Visual Analytics <https://cse6242.gatech.edu/>`_ do Guy Lebannon dạy ở học viện kỹ thuật Georgia.

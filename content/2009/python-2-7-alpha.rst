****************
Python 2.7 alpha
****************


:author: Võ Đức Phương
:category: Tin tức
:date: 2009-12-08 03:38:00
:tags: release, 2.7


Phiên bản thử nghiệm alpha của Python 2.7 đã được phát hành, phiên bản này không có thêm chức năng gì mới mà chủ yếu là sửa chữa lỗi cộng với việc backport các tính năng của python 3000 về lại, đây cũng sẽ là phiên bản cuối cùng của dòng Python 2.

Theo đề xuất mới đây của Guido Van Rossum thì ngôn ngữ Python (cú pháp, thư viện tích hợp v.v...) sẽ tạm dừng phát triển trong vòng ít nhất 2 năm để cho các cài đặt khác của python như PyPy, Jython có thời gian để đuổi kịp nhánh cài đặt chính là CPython. Chính vì vậy mà 2 phiên bản 2.7 và 3.2 sẽ không có thay đổi gì quan trọng mà chỉ chủ yếu để sửa lỗi hay các nâng cấp nhỏ.

Thông tin chi tiết về phiên bản Python 2.7 có tại: http://www.python.org/download/releases/2.7/
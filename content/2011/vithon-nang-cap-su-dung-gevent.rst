******************************
Vithon nâng cấp sử dụng Gevent
******************************


:author: Nhóm PCNV
:category: Tin tức
:date: 2011-08-01 01:25:00
:tags: cgi, wsgi, gevent, vithon


Suốt thời gian qua, nhóm PCNV đã sử dụng chế độ CGI để chạy các ứng dụng web và vẫn đảm bảo được việc truy cập của các bạn quan tâm đến ngôn ngữ Python.

Tuy nhiên, để tránh các vấn đề kỹ thuật có thể phát sinh khi số lượng thành viên tăng lên, cũng như do đòi hỏi của một số tính năng trên diễn đàn, nhóm PCNV đã chuyển hai ứng dụng web là Zine và Vithon Forum qua chạy cùng Gevent, tận dụng chế độ trao đổi không đồng bộ. Việc chuyển đổi này hy vọng sẽ khiến tốc độ truy cập được cải tiến đáng kể.

Nếu các bạn gặp trục trặc trong việc truy cập vào trang PCNV và diễn đàn, thì các bạn hãy thông báo cho chúng tôi theo thông tin liên lạc ở đầu trang.
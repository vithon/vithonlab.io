************************************
Câu chuyện tối ưu hóa đoạn mã Python
************************************


:author: Nguyễn Thành Nam
:category: Tin tức
:date: 2011-10-26 02:21:00
:tags: optimize


Trên trang blog của Dropbox ngày hôm qua có chia sẻ kinh nghiệm của họ trong việc tối ưu hóa một đoạn mã Python ngắn.

http://tech.dropbox.com/?p=89

Kết luận của họ là:

#. Các kỹ thuật căn bản như **inline hàm** (inline function), **tự lặp** (implicit loop), **tận dụng mã C** đều đúng.
#. Cấu trúc **set** và **dict** trong Python rất nhanh.
#. Việc sử dụng biến **nội bộ thay cho toàn cục** mặc dù có ích, nhưng không nhiều.
#. **Ghép chuỗi** (string concatenation) nhanh hơn nhiều so với **định dạng chuỗi** (string interpolation).

Đây là một bài chia sẻ rất hay.
*****************************
Pygments 1.2 với kiểu Monokai
*****************************


:author: Nguyễn Thành Nam
:category: Tin tức
:date: 2010-01-05 10:36:00
:tags: monokai, pygments


Ngày 01 tháng 01 năm 2010 (ngày đầu năm mới 2010), Georg Brandl đã công bố `phiên bản 1.2, tên gọi Neujahr <http://mail.python.org/pipermail/python-announce-list/2010-January/008034.html>`_ của gói Pygments.

Pygments là thư viện làm sáng cú pháp (syntax highlighter) tổng quát, và được sử dụng để tô điểm cho các phần mã nguồn ở chính trang PCNV này.

Người dùng có thể tải Pygments từ http://pypi.python.org/pypi/Pygments, hoặc xem qua một số ví dụ tại http://pygments.org/demo. 

Phiên bản 1.2 không còn tương thích với Python 2.3, có hỗ trợ thêm một số cú pháp mới, và đặc biệt là thêm vào `kiểu màu Monokai <http://www.monokai.nl/blog/2006/07/15/textmate-color-theme/>`_ như các bạn có thể thấy tại trang PCNV.

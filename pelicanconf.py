#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Nhóm PCNV'
SITENAME = u'Python Cho Người Việt'
SITEURL = 'https://www.vithon.org'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Asia/Ho_Chi_Minh'

DEFAULT_LANG = u'vi'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Python.org', 'https://www.python.org/'),)

# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DISPLAY_CATEGORIES_ON_MENU = False
MENUITEMS = (('Bài chỉ dẫn 2.5', 'static/tutorial/2.5/tut.html'),
             ('Tin tức', 'category/tin-tuc.html'),
             ('Bài viết', 'category/bai-viet.html'),
             ('Diễn đàn', 'http://forum.vithon.org'),)

STATIC_PATHS = ARTICLE_EXCLUDES = PAGE_EXCLUDES = ['static']
EXTRA_PATH_METADATA = {
    'static/favicon.ico': {'path': 'favicon.ico'},
    'static/logo.png': {'path': 'logo.png'},
    'static/.nojekyll': {'path': '.nojekyll'},
    'static/CNAME': {'path': 'CNAME'},
}
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
PAGE_URL = 'page/{slug}.html'
PAGE_SAVE_AS = 'page/{slug}.html'
PAGE_LANG_URL = 'page{slug}-{lang}.html'
PAGE_LANG_SAVE_AS = 'page/{slug}-{lang}.html'
ARCHIVES_SAVE_AS = 'archives.html'
AUTHORS_SAVE_AS = 'authors.html'
CATEGORIES_SAVE_AS = 'categories.html'
TAGS_SAVE_AS = 'tags.html'
YEAR_ARCHIVE_SAVE_AS = '{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = '{date:%Y}/{date:%m}/index.html'

THEME = 'theme/plumage'
FAVICON_LINKS = False
TIPUE_SEARCH = True
SITE_THUMBNAIL = 'logo.png'

PLUGIN_PATHS = ['plugin']
PLUGINS = ['tipue_search', 'render_math']
TEMPLATE_PAGES = {
    'search.html': 'search.html',
}

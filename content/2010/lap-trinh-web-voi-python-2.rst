****************************
Lập trình web với Python (2)
****************************


:author: Nguyễn Thành Nam
:category: Bài viết
:date: 2010-01-01 03:05:00
:tags: install


Cài đặt Python
==============

Sau khi cài đặt máy chủ web Apache, chúng ta cần cài đặt trình thông dịch Python. Chúng ta sẽ sử dụng phiên bản 2.6.4 trong chuỗi bài viết này. Bộ cài đặt Python 2.6.4 có thể được tải về từ https://www.python.org/download/.

Khi thực thi phần cài đặt, chúng ta sẽ gặp một hộp thoại như sau:

.. image:: /static/web-programming/install-python/python-install-1.png

Chọn **Install for all users** và nhấn nút **Next** để tiếp tới hình chụp sau:

.. image:: /static/web-programming/install-python/python-install-2.png

Nếu bạn muốn cài đặt vào nơi khác thì chọn đường dẫn, nếu không thì chúng ta sẽ sử dụng đường dẫn mặc định **C:\Python26**. Nhấn nút **Next** để tiếp tục.

.. image:: /static/web-programming/install-python/python-install-3.png

Chúng ta sẽ nhấn tiếp **Next** ở hộp thoại này để bắt đầu quá trình cài đặt các phần thông dụng như trình thông dịch, tài liệu, ví dụ...

.. image:: /static/web-programming/install-python/python-install-4.png

Hộp thoại trên cho chúng ta biết quá trình cài đặt đang diễn ra.

.. image:: /static/web-programming/install-python/python-install-5.png

Khi kết thúc, chúng ta chỉ cần nhấn nút **Finish**.

Để kiểm tra Python có được cài vào máy chưa thì chúng ta sẽ cần mở màn hình dấu nhắc lệnh (command prompt) bằng cách nhấn vào nút **Start** và nhập **cmd.exe** sau đó gõ phím **Enter** như bình bên dưới.

.. image:: /static/web-programming/install-python/python-install-6.png

Chúng ta sẽ nhận được một cửa sổ dòng lệnh. Nhập vào cửa sổ dòng lệnh **c:\Python26\python.exe** thì chúng ta sẽ thấy rằng trình thông dịch Python được thực thi, và nó hiện ra một dấu nhắc tương tác (interactive prompt) như trong hình sau:

.. image:: /static/web-programming/install-python/python-install-7.png

Ngay tại đây chúng ta có thể nhập một câu lệnh Python ``print 'web programming in python'``. Câu lệnh này sẽ được thực hiện và kết quả là chúng ta sẽ nhận được chuỗi **web programming in python** in ra trên màn hình như hình sau:

.. image:: /static/web-programming/install-python/python-install-8.png

Để thoát khỏi trình thông dịch Python, chúng ta sẽ nhập vào dòng lệnh ``quit()``.

.. image:: /static/web-programming/install-python/python-install-9.png

Để thoát khỏi dấu nhắc dòng lệnh thì chúng ta nhập tiếp lệnh **exit**.

Như vậy, chúng ta đã thật sự hoàn tất việc cài đặt trình thông dịch Python. Nếu bạn có nhu cầu tìm hiểu thêm về ngôn ngữ và các thư viện đi kèm thì `bài chỉ dẫn Python 2.5 bằng tiếng Việt </tutorial/2.5/tut.html>`_ có lẽ sẽ giúp ích nhiều cho bạn.

Chúng ta sẽ sẵn sàng cho chương trình web đầu tiên trong bài viết sau! Hẹn gặp lại và chúc mừng năm mới.